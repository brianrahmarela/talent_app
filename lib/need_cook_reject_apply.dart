import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:talent_app/styles/icon.dart';
import 'package:flutter_svg/flutter_svg.dart';

class NeedCookRejectApply extends StatefulWidget {
  const NeedCookRejectApply({Key? key}) : super(key: key);

  @override
  _NeedCookRejectApplyState createState() => _NeedCookRejectApplyState();
}

class _NeedCookRejectApplyState extends State<NeedCookRejectApply> {
  bool val1 = true;
  bool valServiceProvider = true;
  bool valServiceWalter = true;
  bool valServicePainting = true;
  bool valServiceDogWalking = true;

  onChangeFunction1(bool newValue1) {
    val1 = newValue1;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        titleSpacing: 0.0,
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Row(children: [
          Container(
            margin: EdgeInsets.only(left: 50),
            child: Row(
              children: [
                Row(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Text(
                              "Need Cook",
                              style: GoogleFonts.poppins(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                                color: Color(0xffffffff),
                              ),
                            ),
                            Container(
                              alignment: Alignment.topCenter,
                              height: 18,
                              child: SvgPicture.asset(iconAvatarSubscription),
                            ),
                          ],
                        ),
                        Text(
                          "Applied by 18",
                          style: GoogleFonts.poppins(
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                            color: Color(0xffffffff),
                          ),
                        ),
                      ],
                    ),
                    Padding(padding: EdgeInsets.only(right: 78)),
                    Icon(
                      Icons.favorite,
                      color: Colors.white,
                      textDirection: TextDirection.rtl,
                    ),
                    Padding(padding: EdgeInsets.only(right: 23)),
                    Icon(
                      Icons.near_me,
                      color: Colors.white,
                      textDirection: TextDirection.rtl,
                    ),
                  ],
                )
              ],
            ),
          )
        ]),
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/bg_appbar.png'),
            fit: BoxFit.fill,
          ),
        ),
        child: SafeArea(
            child: SizedBox(
          child: ListView(
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(0, 35, 0, 21),
                child: Image(
                  image: AssetImage('assets/images/gallery.png'),
                  // fit: BoxFit.fill,
                ),
              ),
              // buildCardGalleryImg(),
              buildCardJobsDone(),
              buildCardCategory(),
              buildCardCustomer(),
              SizedBox(height: 15),
              buildCardTop(),
              Container(
                // alignment: Alignment.center,
                child: Row(
                  children: [
                    Container(
                      width: 171,
                      // height: 20,
                      margin: EdgeInsets.fromLTRB(17, 32.0, 17, 9),
                      child: SizedBox(
                        // width: 156,
                        child: ElevatedButton(
                          onPressed: () {
                            showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                                  _buildPopupDialog(context),
                            );
                          },
                          style: ElevatedButton.styleFrom(
                              // minimumSize: Size(156, 43),
                              // fixedSize: Size(156, 43),
                              padding: EdgeInsets.zero,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30))),
                          child: Ink(
                            decoration: BoxDecoration(
                                gradient: LinearGradient(colors: [
                                  Color(0xffE4E4E4),
                                  Color(0xffB2B2B2)
                                ]),
                                borderRadius: BorderRadius.circular(30)),
                            child: Container(
                              // width: 156,
                              // margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
                              height: 43,
                              alignment: Alignment.center,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    'REJECT',
                                    overflow: TextOverflow.ellipsis,
                                    style: GoogleFonts.poppins(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w600,
                                      color: Color(0xff000000),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width: 171,
                      // height: 20,
                      margin: EdgeInsets.fromLTRB(17, 32.0, 17, 9),
                      child: SizedBox(
                        // width: 156,
                        child: ElevatedButton(
                          onPressed: () {
                            // print('Hi there');
                          },
                          style: ElevatedButton.styleFrom(
                              // minimumSize: Size(156, 43),
                              // fixedSize: Size(156, 43),
                              padding: EdgeInsets.zero,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20))),
                          child: Ink(
                            decoration: BoxDecoration(
                                gradient: LinearGradient(colors: [
                                  Color(0xff5068A8),
                                  Color(0xff5CB5CF)
                                ]),
                                borderRadius: BorderRadius.circular(30)),
                            child: Container(
                              // width: 156,
                              // margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
                              height: 43,
                              alignment: Alignment.center,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    'ACCEPT',
                                    overflow: TextOverflow.ellipsis,
                                    style: GoogleFonts.poppins(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w600,
                                      color: Color(0xffffffff),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),

              Container(
                child: Row(
                  children: [
                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.fromLTRB(19, 20, 0, 51),
                      child: (Row(children: [
                        SizedBox(height: 10),
                        Padding(
                          padding: EdgeInsets.only(right: 6),
                          child: SvgPicture.asset(iconFlags),
                        ),
                        Text(
                          "Report Trevor S.",
                          style: GoogleFonts.poppins(
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                            color: Color(0xff5068A8),
                          ),
                        ),
                      ])),
                    ),
                  ],
                ),
              )
            ],
          ),
        )),
      ),
    );
  }

  Widget _buildPopupDialog(BuildContext context) {
    return AlertDialog(
      insetPadding: EdgeInsets.all(15),
      contentPadding: EdgeInsets.all(0),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(6))),
      // backgroundColor: Colors.amber,
      // title: const Text('Enter Current Password'),
      // title: Divider(
      //   color: Colors.black,
      //   // height: 17,
      // ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            margin: EdgeInsets.fromLTRB(0, 15, 0, 13.6),
            child: Text("Reason for Cancelling",
                style: GoogleFonts.poppins(
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                )),
          ),
          Divider(
            color: Colors.black,
            // height: 17,
          ),
          Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.fromLTRB(29, 15, 29, 11),
            child: Text("Write your reason below",
                textAlign: TextAlign.center,
                style: GoogleFonts.poppins(
                  fontSize: 12,
                  fontWeight: FontWeight.w600,
                  color: Color(0xff949494),
                )),
          ),
          Container(
              // width: 269.34,
              // height: 59,
              margin: EdgeInsets.fromLTRB(29, 0, 29, 29.4),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // Text('Password',
                  //     textAlign: TextAlign.left,
                  //     style: GoogleFonts.poppins(
                  //       fontSize: 10,
                  //       fontWeight: FontWeight.w500,
                  //       color: Color(0xff949494),
                  //     )),
                  TextField(
                    decoration: InputDecoration(
                      hintText: 'Type here',
                      // labelText: 'Type here',
                      labelStyle: GoogleFonts.poppins(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: Color(0xffB2B2B2),
                      ),
                    ),
                    keyboardType: TextInputType.visiblePassword,
                    // textInputAction: TextInputAction.done,
                    obscureText: true,
                  ),
                ],
              )),
          // Container(
          //   alignment: Alignment.centerRight,
          //   margin: EdgeInsets.fromLTRB(0, 5.5, 30, 0),
          //   child: Text('Forgot Password',
          //       // textAlign: TextAlign.right,
          //       style: GoogleFonts.poppins(
          //         fontSize: 10,
          //         fontWeight: FontWeight.w500,
          //         color: Color(0xff4F78E4),
          //       )),
          // ),
          Row(
            // mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                decoration: BoxDecoration(
                    // color: Colors.grey,
                    borderRadius: BorderRadius.only(
                  bottomLeft: const Radius.circular(6.0),
                )),
                width: 181,
                margin: EdgeInsets.only(top: 25),
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  style: ElevatedButton.styleFrom(
                      padding: EdgeInsets.zero,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20))),
                  child: Ink(
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            colors: [Color(0xffE4E4E4), Color(0xffB2B2B2)]),
                        borderRadius:
                            BorderRadius.only(bottomLeft: Radius.circular(6))),
                    child: Container(
                      width: 378,
                      // margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
                      height: 51,
                      alignment: Alignment.center,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'CANCEL',
                            overflow: TextOverflow.ellipsis,
                            style: GoogleFonts.poppins(
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              color: Color(0xff212121),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                // alignment: Alignment.center,
                width: 181,
                margin: EdgeInsets.only(top: 25),
                // color: Colors.red,
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  style: ElevatedButton.styleFrom(
                      padding: EdgeInsets.zero,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20))),
                  child: Ink(
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            colors: [Color(0xff5068A8), Color(0xff5CB5CF)]),
                        borderRadius:
                            BorderRadius.only(bottomRight: Radius.circular(6))),
                    child: Container(
                      width: 378,
                      // margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
                      height: 51,
                      alignment: Alignment.center,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'VERIFY',
                            overflow: TextOverflow.ellipsis,
                            style: GoogleFonts.poppins(
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              color: Color(0xffffffff),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
      // actions: <Widget>[
      //   TextButton(
      //     onPressed: () {
      //       Navigator.of(context).pop();
      //     },
      //     child: const Text('CANCEL'),
      //   ),
      //   TextButton(
      //     onPressed: () {
      //       Navigator.of(context).pop();
      //     },
      //     child: const Text('ACCEPT'),
      //   ),
      // ],
    );
  }

  Widget chipForRow(String label, Color color) {
    return Chip(
      labelPadding: EdgeInsets.fromLTRB(12.0, 1.0, 12.0, 0.7),
      backgroundColor: Colors.orange.shade300,
      padding: EdgeInsets.all(6.0),
      label: Text(
        label,
        style: TextStyle(color: Colors.white),
      ),
    );
  }

  Widget buildImg(String text) {
    return Container(
      // color: Colors.amber,
      // margin: EdgeInsets.only(left: 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              Positioned(
                child: Container(
                  // color: Colors.black,
                  // height: 70,
                  child: Stack(
                    children: [
                      Positioned(
                          child: Card(
                        elevation: 3.5,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(60.0),
                        ),
                        color: Color(0x212121),
                        child: Container(
                          alignment: Alignment.centerLeft,
                          height: 62,
                          width: 59,
                          child: CircleAvatar(
                            backgroundImage:
                                AssetImage('assets/images/avatar_rating.png'),
                            radius: 60,
                          ),
                        ),
                      )),
                      Positioned(
                          left: 0,
                          top: 38,
                          child: SvgPicture.asset(iconOutlineGreen))
                    ],
                  ),
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(top: 4),
            child: Text(
              text,
              style: GoogleFonts.poppins(
                fontSize: 14,
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 12),
            child: Row(
              children: [
                Icon(
                  Icons.star_rate,
                  color: Colors.yellow.shade700,
                  size: 14.0,
                ),
                Text(
                  " 5",
                  style: GoogleFonts.poppins(
                    fontSize: 11,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget buildImg2(String text) {
    return Container(
      // color: Colors.amber,
      // margin: EdgeInsets.only(left: 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              Positioned(
                child: Container(
                  // color: Colors.black,
                  // height: 70,
                  child: Stack(
                    children: [
                      Positioned(
                          child: Card(
                        elevation: 3.5,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(60.0),
                        ),
                        color: Color(0x212121),
                        child: Container(
                          alignment: Alignment.centerLeft,
                          height: 62,
                          width: 59,
                          child: CircleAvatar(
                            backgroundImage:
                                AssetImage('assets/images/adam1.png'),
                            radius: 30,
                          ),
                        ),
                      )),
                      Positioned(
                          left: 0,
                          top: 38,
                          child: SvgPicture.asset(iconOutlineGreen))
                    ],
                  ),
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(top: 4),
            child: Text(
              text,
              style: GoogleFonts.poppins(
                fontSize: 14,
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 12),
            child: Row(
              children: [
                Icon(
                  Icons.star_rate,
                  color: Colors.yellow.shade700,
                  size: 14.0,
                ),
                Text(
                  " 4",
                  style: GoogleFonts.poppins(
                    fontSize: 11,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget buildImg3(String text) {
    return Container(
      // color: Colors.amber,
      // margin: EdgeInsets.only(left: 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              Positioned(
                child: Container(
                  // color: Colors.black,
                  // height: 70,
                  child: Stack(
                    children: [
                      Positioned(
                          child: Card(
                        elevation: 3.5,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(60.0),
                        ),
                        color: Color(0x212121),
                        child: Container(
                          alignment: Alignment.centerLeft,
                          height: 62,
                          width: 59,
                          child: CircleAvatar(
                            backgroundImage:
                                AssetImage('assets/images/avatar_profile.png'),
                            radius: 30,
                          ),
                        ),
                      )),
                      // Positioned(
                      //     left: 0,
                      //     top: 38,
                      //     child: SvgPicture.asset(iconOutlineGreen))
                    ],
                  ),
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(top: 4),
            child: Text(
              text,
              style: GoogleFonts.poppins(
                fontSize: 14,
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 12),
            child: Row(
              children: [
                Icon(
                  Icons.star_rate,
                  color: Colors.yellow.shade700,
                  size: 14.0,
                ),
                Text(
                  " 3",
                  style: GoogleFonts.poppins(
                    fontSize: 11,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget buildImg4(String text) {
    return Container(
      // color: Colors.amber,
      // margin: EdgeInsets.only(left: 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              Positioned(
                child: Container(
                  // color: Colors.black,
                  // height: 70,
                  child: Stack(
                    children: [
                      Positioned(
                          child: Card(
                        elevation: 3.5,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(60.0),
                        ),
                        color: Color(0x212121),
                        child: Container(
                          alignment: Alignment.centerLeft,
                          height: 62,
                          width: 59,
                          child: CircleAvatar(
                            backgroundImage:
                                AssetImage('assets/images/louis1.png'),
                            radius: 30,
                          ),
                        ),
                      )),
                      // Positioned(
                      //     left: 0,
                      //     top: 38,
                      //     child: SvgPicture.asset(iconOutlineGreen))
                    ],
                  ),
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(top: 4),
            child: Text(
              text,
              style: GoogleFonts.poppins(
                fontSize: 14,
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 12),
            child: Row(
              children: [
                Icon(
                  Icons.star_rate,
                  color: Colors.yellow.shade700,
                  size: 14.0,
                ),
                Text(
                  " 2",
                  style: GoogleFonts.poppins(
                    fontSize: 11,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Card buildCardJobsDone() {
    return Card(
      margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 15),
      shadowColor: Colors.black87,
      elevation: 8,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      color: Colors.white,
      child: Column(
        // mainAxisAlignment: MainAxisAlignment.center,
        // crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 19),
                child: Row(
                  children: [
                    Text(
                      "Budget:",
                      style: GoogleFonts.poppins(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    Padding(padding: EdgeInsets.fromLTRB(0.0, 50.0, 5.0, 0.0)),
                    Text(
                      " \$240",
                      style: GoogleFonts.poppins(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff33B440),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(right: 19),
                child: Row(
                  children: [
                    Icon(
                      Icons.location_pin,
                      color: Colors.red.shade500,
                      size: 16.83,
                    ),
                    Text(
                      " 3 ml ",
                      style: GoogleFonts.poppins(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(left: 19),
            alignment: Alignment.centerLeft,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Need it done before",
                    style: GoogleFonts.poppins(
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                        color: Color(0xffFF0000))),
                Text("10 Jul 2019. 06.44PM",
                    style: GoogleFonts.poppins(
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                    )),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(19.0, 15.0, 19.0, 18.0),
            child: Text(
              "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem dolor sit....",
              style: GoogleFonts.poppins(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff212121)),
              overflow: TextOverflow.ellipsis,
              maxLines: 4,
            ),
          ),
        ],
      ),
    );
  }

  Card buildCardCategory() {
    return Card(
      margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 15),
      shadowColor: Colors.black87,
      elevation: 8,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      color: Colors.white,
      child: Row(
        children: [
          Column(children: [
            Container(
              margin: EdgeInsets.fromLTRB(19, 15, 0, 30),
              width: 160,
              alignment: Alignment.centerLeft,
              // color: Color(0xffaeaeae),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Category",
                    style: GoogleFonts.poppins(
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      // color: Color(0xffffffff),
                    ),
                  ),
                  Text(
                    "Event Planning",
                    style: GoogleFonts.poppins(
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff212121),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: 160,
              margin: EdgeInsets.fromLTRB(19, 0, 0, 30),
              // color: Color(0xffaeaeae),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Job Created on",
                    style: GoogleFonts.poppins(
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      // color: Color(0xffffffff),
                    ),
                  ),
                  Text(
                    "02 Jul 2018, 08.00 AM",
                    style: GoogleFonts.poppins(
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff212121),
                    ),
                  ),
                ],
              ),
            ),
          ]),
          Padding(padding: EdgeInsets.only(right: 35)),
          Column(children: [
            Container(
              margin: EdgeInsets.fromLTRB(0, 15, 0, 30),
              width: 160,
              // color: Color(0xffaeaeae),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Sub Category",
                    style: GoogleFonts.poppins(
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      // color: Color(0xffffffff),
                    ),
                  ),
                  Text(
                    "Cook",
                    style: GoogleFonts.poppins(
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff212121),
                    ),
                  ),
                ],
              ),
            ),
            Stack(
              children: [
                Positioned(
                  child: Container(
                    width: 167,
                    // color: Color(0xffaeaeae),
                    margin: EdgeInsets.fromLTRB(0, 0, 0, 30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Jobs ID",
                          style: GoogleFonts.poppins(
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                            // color: Color(0xffffffff),
                          ),
                        ),
                        Text(
                          "JB5487",
                          style: GoogleFonts.poppins(
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                            color: Color(0xff212121),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  top: 13,
                  left: 112,
                  child: ConstrainedBox(
                    constraints: BoxConstraints.tightFor(width: 54, height: 54),
                    child: ElevatedButton(
                      onPressed: () {
                        // print('Hi there');
                      },
                      style: ElevatedButton.styleFrom(
                          padding: EdgeInsets.all(0),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30))),
                      child: Ink(
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                                colors: [Color(0xff5068A8), Color(0xff5CB5CF)]),
                            borderRadius: BorderRadius.circular(30)),
                        child: Container(
                          alignment: Alignment.center,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SvgPicture.asset(iconChat),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            )
          ]),
        ],
      ),
    );
  }

  Card buildCardCustomer() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
        shadowColor: Colors.black87,
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            margin: EdgeInsets.fromLTRB(19.0, 19.0, 19.0, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Customer's Info",
                  style: GoogleFonts.poppins(
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                    color: Color(0xff000000),
                  ),
                ),
              ],
            ),
          ),
          Container(
            // color: Colors.amberAccent,
            margin: EdgeInsets.only(left: 19),
            // width: 330,
            child: Row(
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  // color: Colors.black,
                  // height: 70,
                  child: Stack(
                    children: [
                      Positioned(
                          child: Container(
                        height: 62,
                        width: 59,
                        alignment: Alignment.centerLeft,
                        // margin: EdgeInsets.only(top: 15),
                        child: CircleAvatar(
                          backgroundImage:
                              AssetImage('assets/images/avatar_rating.png'),
                          radius: 60,
                        ),
                      )),
                      Positioned(
                          left: 0,
                          top: 36,
                          child: SvgPicture.asset(iconCheckGreen))
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(10, 15, 0, 20),
                  width: 200,
                  // color: Colors.red,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            "Trevor S.",
                            style: GoogleFonts.poppins(
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              color: Color(0xff000000),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 4),
                            height: 14,
                            child: SvgPicture.asset(iconCook),
                          ),
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(top: 5.8)),
                      Row(
                        // mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Icon(
                            Icons.star_rate,
                            color: Colors.yellow.shade700,
                            size: 14.0,
                          ),
                          Text(
                            " 5",
                            style: GoogleFonts.poppins(
                              fontSize: 11,
                              fontWeight: FontWeight.w600,
                              // color: Color(0xff33B440),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
                            child: SvgPicture.asset(iconLineVertical),
                          ),
                          Text(
                            "2 Reviews",
                            style: GoogleFonts.poppins(
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                              color: Color(0xff5068A8),
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 2),
                      ),
                      Row(
                        children: [
                          Text(
                            "Other Posted Jobs",
                            style: GoogleFonts.poppins(
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                              color: Color(0xff5068A8),
                            ),
                          ),
                        ],
                      ),
                    ],

                    // Padding(padding: EdgeInsets.only(bottom: 20)),
                  ),
                ),
              ],
            ),
          ),
        ]));
  }

  Card buildCardTop() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
        shadowColor: Colors.black87,
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            margin: EdgeInsets.fromLTRB(19.0, 15.0, 19.0, 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "See People who applied",
                  style: GoogleFonts.poppins(
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                    color: Color(0xff000000),
                  ),
                ),
                ElevatedButton(
                  onPressed: () => {},
                  child: Text(
                    "VIEW ALL",
                    style: GoogleFonts.poppins(
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff5068A8),
                    ),
                  ),
                  style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.all(0),
                    primary: Colors.transparent,
                    // onPrimary: Colors.black,
                    shadowColor: Colors.transparent,
                  ),
                )
                // Text(
                //   "VIEW ALL",
                //   style: GoogleFonts.poppins(
                //     fontSize: 12,
                //     fontWeight: FontWeight.w400,
                //     color: Color(0xff5068A8),
                //   ),
                // ),
              ],
            ),
          ),
          Container(
            // color: Colors.amberAccent,
            margin: EdgeInsets.only(left: 19),
            // width: 330,
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // Container(
                      //   margin: EdgeInsets.only(top: 8),
                      //   child: SizedBox(
                      //     width: 78,
                      //     height: 78,
                      //     child: ElevatedButton(
                      //       onPressed: () {},
                      //       child: SvgPicture.asset(iconAddBlue),
                      //       style: ElevatedButton.styleFrom(
                      //         padding: EdgeInsets.symmetric(
                      //             vertical: 8, horizontal: 30),
                      //         shape: RoundedRectangleBorder(
                      //             borderRadius:
                      //                 BorderRadius.all(Radius.circular(5))),
                      //         primary: Color(0xffC6CEE3),
                      //       ),
                      //     ),
                      //   ),
                      // ),
                      // Padding(padding: EdgeInsets.only(right: 18)),
                      buildImg("Clayton L."),
                      Padding(padding: EdgeInsets.only(right: 36)),
                      buildImg2("Adam"),
                      Padding(padding: EdgeInsets.only(right: 36)),
                      buildImg3("Louis"),
                      Padding(padding: EdgeInsets.only(right: 36)),
                      buildImg4("Theresia."),
                      Padding(padding: EdgeInsets.only(right: 36)),
                      buildImg("05.jpeg"),
                      Padding(padding: EdgeInsets.only(right: 36)),
                      buildImg("06.jpeg"),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ]));
  }

  Card buildCardId() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 15),
        shadowColor: Colors.black87,
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.fromLTRB(19, 15, 18, 0),
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      "Upload ID",
                      style: GoogleFonts.poppins(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff000000),
                      ),
                    ),
                  ],
                ),
                Padding(padding: EdgeInsets.only(bottom: 9.2)),
                Text(
                  "Being ID verified can get you more jobs. This info is not shared with other users",
                  style: GoogleFonts.poppins(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff949494),
                  ),
                ),
                // Padding(padding: EdgeInsets.only(bottom: 18.6)),
              ],
            ),
          ),
          Container(
            width: 500,
            // color: Colors.red,
            margin: EdgeInsets.fromLTRB(19, 9, 0, 17),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 8),
                        child: SizedBox(
                          width: 78,
                          height: 78,
                          child: ElevatedButton(
                            onPressed: () {},
                            child: SvgPicture.asset(iconAddBlue),
                            style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.symmetric(
                                  vertical: 8, horizontal: 30),
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5))),
                              primary: Color(0xffC6CEE3),
                            ),
                          ),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(right: 18)),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ]));
  }

  Card buildCardGalleryImg() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 15),
        shadowColor: Colors.black87,
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            // color: Color(0xff000000),

            // margin: EdgeInsets.fromLTRB(19, 24.6, 19, 15.0),
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/gallery.png'),
                fit: BoxFit.fill,
              ),
            ),
          ),
        ]));
  }
}
