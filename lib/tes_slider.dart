import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:talent_app/widget/slider_image.dart';
import 'package:google_fonts/google_fonts.dart';

class TesSlider extends StatefulWidget {
  const TesSlider({Key? key}) : super(key: key);

  @override
  _TesSliderState createState() => _TesSliderState();
}

class _TesSliderState extends State<TesSlider> {
  PageController controller =
      PageController(initialPage: 0, viewportFraction: 0.26);

  List<String> urls = [
    "https://images.unsplash.com/photo-1604223190546-a43e4c7f29d7?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8bW91bnRhaW4lMjBsYW5kc2NhcGV8ZW58MHx8MHx8&ixlib=rb-1.2.1&w=1000&q=80",
    "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/20027915-1bcacf85634dfa9af0036657a6797e7b.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit",
    "https://images.unsplash.com/photo-1604223190546-a43e4c7f29d7?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8bW91bnRhaW4lMjBsYW5kc2NhcGV8ZW58MHx8MHx8&ixlib=rb-1.2.1&w=1000&q=80",
    "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/20027915-1bcacf85634dfa9af0036657a6797e7b.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit",
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          // backgroundColor: Colors.transparent,
          elevation: 0,
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back_ios),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text(
            "Rating & Review",
            style:
                GoogleFonts.poppins(fontSize: 18, fontWeight: FontWeight.w600),
          ),
        ),
        body: Container(
          child: PageView.builder(
              controller: controller,
              itemCount: urls.length,
              itemBuilder: (context, index) => Row(
                    children: [
                      SliderImage(urls[index]),
                    ],
                  )),
        ));
  }
}
