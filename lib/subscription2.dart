import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_svg/flutter_svg.dart';
import './styles/icon.dart';

class Subscription2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        // backgroundColor: Color(0x44000000),
        elevation: 0,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          "Subscription",
          style: GoogleFonts.poppins(fontSize: 18, fontWeight: FontWeight.w600),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          // height: 760,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/bg_subscription.png'),
              fit: BoxFit.fill,
            ),
          ),

          child: SafeArea(
              child: Center(
                  child: Column(
            children: [
              Container(
                // margin: EdgeInsets.only(top: 92),
                child: buildCardHiddenTop(),
                width: 530,
                // height: 66,
              ),
              Divider(
                color: Colors.white,
                // height: 17,
              ),
              Container(
                margin: EdgeInsets.fromLTRB(17.0, 15.0, 0, 0),
                child: Row(
                  children: [
                    Text("Your current plan is",
                        style: GoogleFonts.poppins(
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                            color: Colors.white)),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(0, 15.0, 0, 15.0),
                child: buildCardTop(),
                width: 530,
                height: 84,
              ),
              Divider(
                color: Colors.white,
                // height: 17,
              ),
              Container(
                margin: EdgeInsets.fromLTRB(17.0, 15.0, 0, 0),
                child: Row(
                  children: [
                    Text("Upgrade to a yearly subscriptions",
                        style: GoogleFonts.poppins(
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                            color: Colors.white)),
                  ],
                ),
              ), //   height: 10,
              // ),
              Container(
                margin: EdgeInsets.only(top: 15),
                child: buildCardBtm(),
                width: 530,
                height: 84,
              ),
              // SizedBox(
              //   height: 30,
              // ),
              Container(
                margin: EdgeInsets.only(top: 92),
                child: buildCardHidden(),
                width: 530,
                height: 75,
              ),
              Container(
                margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
                child: SizedBox(
                  // width: 330,
                  // height: 43,
                  child: ElevatedButton(
                    onPressed: () {
                      // print('Hi there');
                    },
                    style: ElevatedButton.styleFrom(
                        padding: EdgeInsets.zero,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20))),
                    child: Ink(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              colors: [Color(0xff5068A8), Color(0xff5CB5CF)]),
                          borderRadius: BorderRadius.circular(30)),
                      child: Container(
                        // width: 330,
                        // margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
                        height: 43,
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'ADD CREDIT',
                              overflow: TextOverflow.ellipsis,
                              style: GoogleFonts.poppins(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                color: Color(0xffffffff),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 9),
                              child: SvgPicture.asset(iconAdd),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                // margin: EdgeInsets.only(bottom: 33),
                child: buildCardFooter(),
                width: 530,
                height: 56,
              ),
            ],
          ))),
        ),
      ),
    );
  }

  Card buildCardTop() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
        shadowColor: Colors.black87,
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
        color: Colors.white,
        child: Column(children: [
          Container(
            margin: EdgeInsets.fromLTRB(19.0, 17.0, 19.0, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Monthly",
                  style: GoogleFonts.poppins(
                    fontSize: 23,
                    fontWeight: FontWeight.bold,
                    color: Color(0xff000000),
                  ),
                ),
                Text(
                  "10 pts.",
                  style: GoogleFonts.poppins(
                    fontSize: 23,
                    fontWeight: FontWeight.bold,
                    color: Color(0xff000000),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(19.0, 2.0, 19.0, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Subscription",
                  style: GoogleFonts.poppins(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff949494),
                  ),
                ),
                Text(
                  "/month",
                  style: GoogleFonts.poppins(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff949494),
                  ),
                ),
              ],
            ),
          ),
        ]));
  }

  Card buildCardBtm() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
        shadowColor: Colors.black87,
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
        color: Colors.white,
        child: Column(children: [
          Container(
            margin: EdgeInsets.fromLTRB(19.0, 17.0, 19.0, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Yearly",
                  style: GoogleFonts.poppins(
                    fontSize: 23,
                    fontWeight: FontWeight.bold,
                    color: Color(0xff000000),
                  ),
                ),
                Text(
                  "80 pts.",
                  style: GoogleFonts.poppins(
                    fontSize: 23,
                    fontWeight: FontWeight.bold,
                    color: Color(0xff000000),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(19.0, 2.0, 19.0, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Subscription",
                  style: GoogleFonts.poppins(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff949494),
                  ),
                ),
                Text(
                  "/year",
                  style: GoogleFonts.poppins(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff949494),
                  ),
                ),
              ],
            ),
          ),
        ]));
  }

  Card buildCardHidden() {
    return Card(
        margin: EdgeInsets.fromLTRB(15, 0, 15, 0),
        elevation: 0,
        color: Colors.transparent,
        // shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
        child: Column(children: [
          Container(
            margin: EdgeInsets.only(top: 17.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "96",
                  style: GoogleFonts.poppins(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Color(0xff000000),
                  ),
                ),
                Text(
                  "31 Oct 2018",
                  style: GoogleFonts.poppins(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Color(0xff000000),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 2.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Points",
                  style: GoogleFonts.poppins(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                    color: Color(0xff000000),
                  ),
                ),
                Text(
                  "Expire On",
                  style: GoogleFonts.poppins(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                    color: Color(0xff000000),
                  ),
                ),
              ],
            ),
          ),
        ]));
  }

  Card buildCardHiddenTop() {
    return Card(
        margin: EdgeInsets.fromLTRB(0, 26, 0, 30),
        elevation: 0,
        color: Colors.transparent,
        // shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
        child: Column(children: [
          Container(
            // margin: EdgeInsets.only(top: 17.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.only(bottom: 5),
                  child: SvgPicture.asset(iconAvatarSubscription),
                ),
                Text(
                  "You are Premium Member",
                  style: GoogleFonts.poppins(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                    color: Color(0xffffffff),
                  ),
                ),
                // Text(
                //   "31 Oct 2018",
                //   style: GoogleFonts.poppins(
                //     fontSize: 20,
                //     fontWeight: FontWeight.bold,
                //     color: Color(0xff000000),
                //   ),
                // ),
              ],
            ),
          ),
        ]));
  }

  Card buildCardFooter() {
    return Card(
        // margin: EdgeInsets.fromLTRB(40, 0, 40, 0),
        elevation: 0,
        color: Colors.transparent,
        // shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
        child: Column(children: [
          Container(
            margin: EdgeInsets.fromLTRB(19.0, 0, 19.0, 20.0),
            // padding: EdgeInsets.all(9),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.only(right: 9),
                  child: SvgPicture.asset(iconWallet),
                ),
                Padding(
                  padding: EdgeInsets.only(right: 9),
                  child: SvgPicture.asset(iconPaypalFigma),
                ),
                Text(
                  "Accept Paypal & All Credit & Debit Cards",
                  style: GoogleFonts.poppins(
                    fontSize: 10,
                    fontWeight: FontWeight.w600,
                    color: Color(0xff000000),
                  ),
                ),
              ],
            ),
          ),
        ]));
  }
}
