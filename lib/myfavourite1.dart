import 'dart:ui';

import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:talent_app/styles/icon.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'myfavourite2.dart';
import 'widget/slidable_myfavourite.dart';

class MyFavourite extends StatefulWidget {
  const MyFavourite({Key? key}) : super(key: key);

  @override
  _MyFavouriteState createState() => _MyFavouriteState();
}

class _MyFavouriteState extends State<MyFavourite> {
  bool valServiceWalter = true;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          extendBodyBehindAppBar: true,
          backgroundColor: Colors.white,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: new IconButton(
              icon: new Icon(Icons.arrow_back_ios),
              onPressed: () => Navigator.of(context).pop(),
            ),
            title: Text(
              "My Favourites",
              style: GoogleFonts.poppins(
                  fontSize: 18, fontWeight: FontWeight.w600),
            ),
            bottom: TabBar(
              labelStyle: GoogleFonts.poppins(
                  fontSize: 16, fontWeight: FontWeight.w400), //For Selected tab

              unselectedLabelColor: Colors.white54,
              // indicatorColor: Color(0xffffffff),
              indicator: UnderlineTabIndicator(
                  // borderSide: BorderSide(width: 2.0),
                  insets: EdgeInsets.symmetric(horizontal: 70.0),
                  borderSide: BorderSide(color: Colors.white, width: 3)),
              // indicatorSize: TabBarIndicatorSize.label,
              // isScrollable: true,
              tabs: [
                Tab(text: "Jobs"),
                Tab(text: "Service Providers"),
                // Tab(text: "Booked Jobs"),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              Container(
                height: 760,
                // padding: EdgeInsets.only(bottom: 50),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/images/bg_myjob.png'),
                    fit: BoxFit.fill,
                  ),
                ),
                child: SafeArea(
                    child: ListView(
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 46, 0, 15),
                      child: SlidableWidget(
                        onDismissed: (SlidableAction action) {
                          setState(() {
                            // items.removeAt(index);
                          });
                        },
                        key: Key(""),
                        child: Container(
                          margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          child: buildCardSelectService(),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 15),
                      child: SlidableWidget(
                        onDismissed: (SlidableAction action) {
                          setState(() {
                            // items.removeAt(index);
                          });
                        },
                        key: Key(""),
                        child: Container(
                          margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          child: buildCardSelectService(),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 15),
                      child: SlidableWidget(
                        onDismissed: (SlidableAction action) {
                          setState(() {
                            // items.removeAt(index);
                          });
                        },
                        key: Key(""),
                        child: Container(
                          margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          child: buildCardSelectService(),
                        ),
                      ),
                    ),
                    SizedBox(height: 55),
                  ],
                )),
              ),
              MyFavourite2(),
            ],
          ),
        ));
  }

  Card buildCardSelectService() {
    return Card(
      margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
      shadowColor: Colors.black87,
      elevation: 8,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
      color: Colors.white,
      child: Column(
        children: [
          buildCardWalterNeedCook("Need Cook"),
          Container(
            margin: EdgeInsets.zero,
            padding: EdgeInsets.zero,
            // height: 100,
            child: Row(
              children: [
                Container(
                  // alignment: Alignment.topLeft,
                  // color: Colors.blue,
                  width: 381,
                  child: Row(
                    children: [
                      Image(
                        image: AssetImage('assets/images/img_myfav.png'),
                        // fit: BoxFit.fill,
                      ),
                      Column(
                        children: [
                          Container(
                            width: 230,
                            // color: Colors.amber,
                            margin: EdgeInsets.fromLTRB(0, 5.0, 0, 0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 5),
                                  child: Text(
                                    "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit",
                                    maxLines: 4,
                                    overflow: TextOverflow.ellipsis,
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Color(0xff000000),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(padding: EdgeInsets.only(top: 15)),
                          Container(
                            width: 230,
                            // color: Colors.amber,
                            // margin: EdgeInsets.fromLTRB(19.0, 17.0, 19.0, 0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(right: 9),
                                  child: SvgPicture.asset(iconTv),
                                ),
                                Text(
                                  "Before the 08 Nov 2018.",
                                  style: GoogleFonts.poppins(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500,
                                    color: Color(0xff000000),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 230,
                            // color: Colors.amber,
                            // margin: EdgeInsets.fromLTRB(19.0, 17.0, 19.0, 0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(children: [
                                  Padding(
                                    padding: EdgeInsets.only(right: 9),
                                    child: SvgPicture.asset(iconPlace),
                                  ),
                                  Text(
                                    "3 ml",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500,
                                      color: Color(0xff000000),
                                    ),
                                  )
                                ]),
                                SizedBox(height: 32),
                                Row(children: [
                                  Text(
                                    "Budget:",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w600,
                                      color: Color(0xff000000),
                                    ),
                                  ),
                                  Text(
                                    "\$240",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w600,
                                      color: Color(0xff33B440),
                                    ),
                                  ),
                                ]),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 21,
          ),
        ],
      ),
    );
  }

  Card buildCardWalterNeedCook(String text1) {
    return Card(
      margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
      shadowColor: Colors.black87,
      elevation: 0,
      // color: Colors.green,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
      child: Container(
        // color: Colors.amber,
        child: Row(
          // crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: Row(
                children: [
                  Stack(
                    children: [
                      Positioned(
                        child: Container(
                          width: 150,
                          height: 25,
                          alignment: Alignment.topLeft,
                          // color: Colors.red,
                          margin: EdgeInsets.fromLTRB(15, 0, 0, 0),
                          // padding: EdgeInsets.all(0),
                          child: (Column(
                              // mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: 0),
                                Text(
                                  text1,
                                  style: GoogleFonts.poppins(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500,
                                    color: Color(0xff000000),
                                  ),
                                ),
                              ])),
                        ),
                      ),
                      Positioned(
                          left: 95,
                          top: 0,
                          child: Padding(
                            padding: EdgeInsets.only(left: 8),
                            child: SvgPicture.asset(iconCook),
                          )),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 10, 17, 13),
              child: SizedBox(
                // width: 330,
                height: 30,
                child: ElevatedButton(
                  onPressed: () {
                    // print('Hi there');
                  },
                  style: ElevatedButton.styleFrom(
                      padding: EdgeInsets.zero,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30))),
                  child: Ink(
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            colors: [Color(0xff5068A8), Color(0xff5CB5CF)]),
                        borderRadius: BorderRadius.circular(30)),
                    child: Container(
                      width: 79,
                      // margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
                      height: 30,
                      alignment: Alignment.center,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Apply',
                            overflow: TextOverflow.ellipsis,
                            style: GoogleFonts.poppins(
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              color: Color(0xffffffff),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildSwitchServiceWalter() => Transform.scale(
        scale: 0.6,
        child: CupertinoSwitch(
          activeColor: Color(0xff5068A8),
          value: valServiceWalter,
          onChanged: (valServiceWalter) =>
              setState(() => this.valServiceWalter = valServiceWalter),
        ),
      );
}
