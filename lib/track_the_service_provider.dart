// import 'dart:html';
import 'dart:ui';

import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:talent_app/styles/icon.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class TrackServiceProvider extends StatefulWidget {
  const TrackServiceProvider({Key? key}) : super(key: key);

  @override
  _TrackServiceProviderState createState() => _TrackServiceProviderState();
}

class _TrackServiceProviderState extends State<TrackServiceProvider> {
  bool valServiceWalter = true;

  //marker google maps
  final Set<Marker> _markers = {};
  final LatLng _currentPosition = LatLng(-6.209325, 106.819555);

  // @override
  // void initState() {
  //   _markers.add(Marker(
  //     markerId: MarkerId("-6.209325, 106.819555"),
  //     position: _currentPosition,
  //     icon: BitmapDescriptor.defaultMarker,
  //   ));
  //   super.initState();
  // }
  void _onMapCreated(GoogleMapController controller) {
    setState(() {
      _markers.add(
        Marker(
            markerId: MarkerId('id-1'),
            position: _currentPosition,
            infoWindow: InfoWindow(
              title: 'You Are Here',
              // snippet: 'Tes',
            )),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          "Track the Service Provider",
          style: GoogleFonts.poppins(fontSize: 18, fontWeight: FontWeight.w600),
        ),
      ),
      body: Container(
        height: 760,
        // padding: EdgeInsets.only(bottom: 50),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/bg_booking.png'),
            fit: BoxFit.fill,
          ),
        ),
        child: SafeArea(
          child: Container(
            margin: EdgeInsets.fromLTRB(0, 46, 0, 15),
            child: Stack(
              children: [
                // Positioned(
                Container(
                  child: GoogleMap(
                    mapType: MapType.normal,
                    onMapCreated: _onMapCreated,
                    initialCameraPosition: CameraPosition(
                        target: _currentPosition,
                        tilt: 59.440717697143555,
                        zoom: 19.151926040649414),
                    markers: _markers,
                    onTap: (position) {
                      setState(() {
                        _markers.add(Marker(
                          markerId: MarkerId(
                              "${position.latitude},${position.longitude}"),
                          icon: BitmapDescriptor.defaultMarker,
                          position: position,
                        ));
                      });
                    },
                  ),
                ),
                // ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Card buildCardTop() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
        shadowColor: Colors.black87,
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            // color: Colors.amberAccent,
            margin: EdgeInsets.fromLTRB(19.0, 17.0, 19.0, 0),
            // width: 330,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  // color: Colors.black,
                  // height: 70,
                  child: Stack(
                    children: [
                      Positioned(
                          child: Card(
                        elevation: 3.5,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(60.0),
                        ),
                        color: Color(0x212121),
                        child: Container(
                          alignment: Alignment.centerLeft,
                          height: 59,
                          width: 59,
                          child: CircleAvatar(
                            backgroundImage:
                                AssetImage('assets/images/avatar_rating.png'),
                            radius: 60,
                          ),
                        ),
                      )),
                      Positioned(
                          left: 0,
                          top: 38,
                          child: SvgPicture.asset(iconOutlineGreen))
                    ],
                  ),
                ),
                Container(
                  // color: Color(0xffaeaeae),
                  margin: EdgeInsets.only(left: 8),
                  width: 200,
                  // color: Colors.red,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            "Clayton L",
                            style: GoogleFonts.poppins(
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              color: Color(0xff000000),
                            ),
                          ),
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(top: 4.7)),
                      Row(
                        // mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Icon(
                            Icons.star_rate,
                            color: Colors.yellow.shade700,
                            size: 14.0,
                          ),
                          Text(
                            " 5",
                            style: GoogleFonts.poppins(
                              fontSize: 11,
                              fontWeight: FontWeight.w600,
                              // color: Color(0xff33B440),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 5),
                      Row(
                        children: [
                          Text(
                            "Plumber & 5 More",
                            style: GoogleFonts.poppins(
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                              color: Color(0xff949494),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            // color: Color,
            height: 39,
            margin: EdgeInsets.only(top: 32),
            padding: EdgeInsets.zero,
            child: Row(
              children: [
                Flexible(
                  flex: 1,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    style: ElevatedButton.styleFrom(
                        padding: EdgeInsets.zero,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20))),
                    child: Ink(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              colors: [Color(0xffE4E4E4), Color(0xffB2B2B2)]),
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(10))),
                      child: Container(
                        // width: 378,
                        margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        // height: 47,
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'REJECT',
                              overflow: TextOverflow.ellipsis,
                              style: GoogleFonts.poppins(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                color: Color(0xff212121),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Flexible(
                  flex: 2,

                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    style: ElevatedButton.styleFrom(
                        padding: EdgeInsets.zero,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20))),
                    child: Ink(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              colors: [Color(0xff5068A8), Color(0xff5CB5CF)]),
                          borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(10))),
                      child: Container(
                        // width: 378,
                        // margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
                        padding: EdgeInsets.zero,
                        // height: 47,
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'ACCEPT',
                              overflow: TextOverflow.ellipsis,
                              style: GoogleFonts.poppins(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                color: Color(0xffffffff),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  // ),
                ),
              ],
            ),
          )
        ]));
  }
}
