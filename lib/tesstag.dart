import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

const List<StaggeredTile> _staggeredTiles = <StaggeredTile>[
  StaggeredTile.count(2, 2),
  StaggeredTile.count(1, 1),
  StaggeredTile.count(1, 1),
  StaggeredTile.count(1, 1),
  StaggeredTile.count(1, 1),
  StaggeredTile.count(1, 2),
  StaggeredTile.count(1, 1),
  StaggeredTile.count(3, 1),
  StaggeredTile.count(1, 1),
  StaggeredTile.count(4, 1),
];

const List<Widget> _tiles = <Widget>[
  _Example01Tile(Colors.green, Icons.widgets),
  _Example02Tile(Colors.lightBlue, Icons.wifi),
  _Example01Tile(Colors.amber, Icons.panorama_wide_angle),
  _Example01Tile(Colors.brown, Icons.map),
  _Example01Tile(Colors.deepOrange, Icons.send),
  // _Example01Tile(Colors.indigo, Icons.airline_seat_flat),
  // // _Example01Tile(Colors.red, Icons.bluetooth),
  // _Example01Tile(Colors.pink, Icons.battery_alert),
  // _Example01Tile(Colors.purple, Icons.desktop_windows),
  // _Example01Tile(Colors.blue, Icons.radio),
];

class Example01 extends StatefulWidget {
  @override
  _Example01State createState() => _Example01State();
}

class _Example01State extends State<Example01> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('example 01'),
        ),
        body: Padding(
            padding: const EdgeInsets.only(top: 12),
            child: StaggeredGridView.count(
              crossAxisCount: 4,
              staggeredTiles: _staggeredTiles,
              mainAxisSpacing: 0,
              crossAxisSpacing: 0,
              padding: const EdgeInsets.fromLTRB(19, 0, 19, 0),
              children: _tiles,
            )));
  }
}

class _Example01Tile extends StatefulWidget {
  const _Example01Tile(this.backgroundColor, this.iconData);

  final Color backgroundColor;
  final IconData iconData;

  @override
  __Example01TileState createState() => __Example01TileState();
}

class __Example01TileState extends State<_Example01Tile> {
  @override
  Widget build(BuildContext context) {
    return Card(
      color: widget.backgroundColor,
      child: InkWell(
        onTap: () {},
        child: Center(
          child: Padding(
              padding: const EdgeInsets.all(4),
              child: Image(image: AssetImage("assets/images/food_3.jpg"))),
        ),
      ),
    );
  }
}

class _Example02Tile extends StatelessWidget {
  const _Example02Tile(this.backgroundColor, this.iconData);

  final Color backgroundColor;
  final IconData iconData;

  @override
  Widget build(BuildContext context) {
    return Card(
      color: backgroundColor,
      child: InkWell(
        onTap: () {},
        child: Center(
          child: Padding(
              padding: const EdgeInsets.all(4),
              child: Image(image: AssetImage("assets/images/food_2.jpg"))),
        ),
      ),
    );
  }
}
