import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:talent_app/styles/icon.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Chats2 extends StatefulWidget {
  const Chats2({Key? key}) : super(key: key);

  @override
  _Chats2State createState() => _Chats2State();
}

class _Chats2State extends State<Chats2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: true,
        backgroundColor: Colors.white,
        appBar: AppBar(
          titleSpacing: 0.0,
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back_ios),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Row(children: [
            Container(
              margin: EdgeInsets.only(left: 50),
              child: Row(
                children: [
                  Row(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Text(
                                "Michael Y.",
                                style: GoogleFonts.poppins(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                  color: Color(0xffffffff),
                                ),
                              ),
                              Container(
                                alignment: Alignment.topCenter,
                                height: 18,
                                child: SvgPicture.asset(iconAvatarSubscription),
                              ),
                            ],
                          ),
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(right: 100)),
                      ElevatedButton(
                        onPressed: () => {},
                        child: SvgPicture.asset(iconAddUser),
                        style: ElevatedButton.styleFrom(
                          padding: EdgeInsets.zero,
                          primary: Colors.transparent,
                          // onPrimary: Colors.black,
                          shadowColor: Colors.transparent,
                        ),
                      )
                    ],
                  )
                ],
              ),
            )
          ]),
        ),
        body: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/bg_appbar.png'),
                  fit: BoxFit.fill,
                ),
              ),
              child: SafeArea(
                  child: SizedBox(
                child: ListView(
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 51, 0, 18),
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          // Note: Styles for TextSpans must be explicitly defined.
                          // Child text spans will inherit styles from parent
                          style: GoogleFonts.poppins(
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            letterSpacing: 0.3,
                          ),
                          children: <TextSpan>[
                            TextSpan(
                                text: 'Job Title',
                                style: TextStyle(
                                  color: Color(0xff707070),
                                )),
                            TextSpan(
                                text: ' Need a Cook',
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color: Color(0xff212121),
                                )),
                          ],
                        ),
                      ),
                    ),
                    Divider(
                      color: Colors.black87,
                      height: 0,
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(13, 2, 13, 2),
                      margin: EdgeInsets.fromLTRB(145, 0, 145, 0),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(6),
                          color: Color(0xffE4E4E4),
                          border: Border.all(color: Color(0xffB2B2B2))),
                      alignment: Alignment.center,
                      child: Text(
                        "Today",
                        style: GoogleFonts.poppins(
                          fontSize: 11,
                          fontWeight: FontWeight.w500,
                          color: Color(0xff7A7878),
                          letterSpacing: 0.3,
                        ),
                      ),
                    ),
                    Row(
                      children: [
                        Container(
                          margin: EdgeInsets.fromLTRB(19, 60, 11, 0),
                          child: Column(
                            children: [
                              CircleAvatar(
                                backgroundImage:
                                    AssetImage('assets/images/adam1.png'),
                                radius: 30,
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(5, 60, 19, 0),
                          padding: EdgeInsets.fromLTRB(15, 15, 15, 0),
                          width: 293,
                          height: 94,
                          decoration: BoxDecoration(
                              gradient: LinearGradient(colors: [
                                Color(0xff5068A8),
                                Color(0xff67C9E6)
                              ]),
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(10),
                                  topRight: Radius.circular(10),
                                  bottomRight: Radius.circular(10),
                                  bottomLeft: Radius.circular(0))),
                          child: Column(
                            children: [
                              Text(
                                "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem dolor sit....",
                                style: GoogleFonts.poppins(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xffffffff)),
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                              ),
                              SizedBox(
                                height: 4,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text(
                                    "22.30 PM",
                                    style: GoogleFonts.poppins(
                                        fontSize: 10.5,
                                        fontWeight: FontWeight.w600,
                                        color: Color(0xffffffff)),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Container(
                          margin: EdgeInsets.fromLTRB(19, 15, 11, 0),
                          padding: EdgeInsets.fromLTRB(15, 15, 15, 0),
                          width: 293,
                          height: 94,
                          decoration: BoxDecoration(
                              color: Color(0xffC6CEE3),
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(10),
                                  topRight: Radius.circular(10),
                                  bottomRight: Radius.circular(10),
                                  bottomLeft: Radius.circular(0))),
                          child: Column(
                            children: [
                              Text(
                                "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem dolor sit....",
                                style: GoogleFonts.poppins(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xffffffff)),
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                              ),
                              SizedBox(
                                height: 4,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text(
                                    "22.30 PM",
                                    style: GoogleFonts.poppins(
                                        fontSize: 10.5,
                                        fontWeight: FontWeight.w600,
                                        color: Color(0xffffffff)),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(5, 15, 19, 0),
                          child: Column(
                            children: [
                              CircleAvatar(
                                backgroundImage: AssetImage(
                                    'assets/images/avatar_rating.png'),
                                radius: 30,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 145,
                    ),
                    bottomNavBar(),
                  ],
                ),
              )),
            )
          ],
        ));
  }

  Widget bottomNavBar() {
    return Card(
        margin: EdgeInsets.zero,
        elevation: 8,
        child: BottomNavigationBar(
          type: BottomNavigationBarType.shifting,
          backgroundColor: Color(0xFFffffff),
          selectedItemColor: Colors.white,
          unselectedItemColor: Colors.white.withOpacity(.60),
          selectedFontSize: 14,
          unselectedFontSize: 14,
          elevation: 0.0,
          onTap: (value) {
            // Respond to item press.
          },
          items: [
            BottomNavigationBarItem(
                icon: Container(
                  // color: Color(0xffaeaeae),
                  margin: EdgeInsets.fromLTRB(19, 0, 0, 0),
                  child: TextField(
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Type here',
                      // labelText: 'Type here',
                      labelStyle: GoogleFonts.poppins(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: Color(0xffB2B2B2),
                      ),
                    ),
                    keyboardType: TextInputType.text,
                    // textInputAction: TextInputAction.done,
                    // obscureText: true,
                  ),
                ),
                label: "tes"),
            BottomNavigationBarItem(
                icon: Container(
                  alignment: Alignment.centerRight,
                  // color: Color(0xffaeaeae),
                  margin: EdgeInsets.fromLTRB(19, 0, 19, 0),
                  child: SvgPicture.asset(iconMicrophone),
                ),
                label: "tes"),
            // BottomNavigationBarItem(
            //   label: 'Music',
            //   icon: Icon(Icons.music_note),
            // ),
          ],
          selectedIconTheme: IconThemeData(opacity: 0.0, size: 0),
          unselectedIconTheme: IconThemeData(opacity: 0.0, size: 0),
          showSelectedLabels: false,
          showUnselectedLabels: false,
        ));
  }
}
