import 'dart:ui';

import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:talent_app/styles/icon.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'widget/slideable_myjob.dart';

class MyJobs3 extends StatefulWidget {
  const MyJobs3({Key? key}) : super(key: key);

  @override
  _MyJobs3State createState() => _MyJobs3State();
}

class _MyJobs3State extends State<MyJobs3> {
  bool valServiceWalter = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.white,
      body: Container(
        height: 760,
        // padding: EdgeInsets.only(bottom: 50),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/bg_myjob.png'),
            fit: BoxFit.fill,
          ),
        ),
        child: SafeArea(
            child: ListView(
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(0, 46, 0, 15),
              child: SlidableWidget(
                onDismissed: (SlidableAction action) {
                  setState(() {
                    // items.removeAt(index);
                  });
                },
                key: Key(""),
                child: Container(
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                  child: buildCardSelectService(),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 15),
              child: SlidableWidget(
                onDismissed: (SlidableAction action) {
                  setState(() {
                    // items.removeAt(index);
                  });
                },
                key: Key(""),
                child: Container(
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                  child: buildCardSelectService(),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 15),
              child: SlidableWidget(
                onDismissed: (SlidableAction action) {
                  setState(() {
                    // items.removeAt(index);
                  });
                },
                key: Key(""),
                child: Container(
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                  child: buildCardSelectService(),
                ),
              ),
            ),
            SizedBox(height: 55),
          ],
        )),
      ),
    );
  }

  Widget _buildPopupDialog(BuildContext context) {
    return AlertDialog(
      insetPadding: EdgeInsets.all(15),
      contentPadding: EdgeInsets.all(0),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(6))),
      // backgroundColor: Colors.amber,
      // title: const Text('Enter Current Password'),
      // title: Divider(
      //   color: Colors.black,
      //   // height: 17,
      // ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            margin: EdgeInsets.fromLTRB(0, 15, 0, 13.6),
            child: Text("Reason for Cancelling",
                style: GoogleFonts.poppins(
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                )),
          ),
          Divider(
            color: Colors.black,
            // height: 17,
          ),
          Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.fromLTRB(29, 15, 29, 11),
            child: Text("Write your reason below",
                textAlign: TextAlign.center,
                style: GoogleFonts.poppins(
                  fontSize: 12,
                  fontWeight: FontWeight.w600,
                  color: Color(0xff949494),
                )),
          ),
          Container(
              // width: 269.34,
              // height: 59,
              margin: EdgeInsets.fromLTRB(29, 0, 29, 29.4),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // Text('Password',
                  //     textAlign: TextAlign.left,
                  //     style: GoogleFonts.poppins(
                  //       fontSize: 10,
                  //       fontWeight: FontWeight.w500,
                  //       color: Color(0xff949494),
                  //     )),
                  TextField(
                    decoration: InputDecoration(
                      hintText: 'Type here',
                      // labelText: 'Type here',
                      labelStyle: GoogleFonts.poppins(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: Color(0xffB2B2B2),
                      ),
                    ),
                    keyboardType: TextInputType.visiblePassword,
                    // textInputAction: TextInputAction.done,
                    obscureText: true,
                  ),
                ],
              )),
          // Container(
          //   alignment: Alignment.centerRight,
          //   margin: EdgeInsets.fromLTRB(0, 5.5, 30, 0),
          //   child: Text('Forgot Password',
          //       // textAlign: TextAlign.right,
          //       style: GoogleFonts.poppins(
          //         fontSize: 10,
          //         fontWeight: FontWeight.w500,
          //         color: Color(0xff4F78E4),
          //       )),
          // ),
          Row(
            // mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                // alignment: Alignment.center,
                width: 330,
                margin: EdgeInsets.only(top: 25),
                // color: Colors.red,
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  style: ElevatedButton.styleFrom(
                      padding: EdgeInsets.zero,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20))),
                  child: Ink(
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            colors: [Color(0xff5068A8), Color(0xff5CB5CF)]),
                        borderRadius:
                            BorderRadius.only(bottomRight: Radius.circular(6))),
                    child: Container(
                      width: 378,
                      // margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
                      height: 51,
                      alignment: Alignment.center,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'CANCEL THE JOB',
                            overflow: TextOverflow.ellipsis,
                            style: GoogleFonts.poppins(
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              color: Color(0xffffffff),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
      // actions: <Widget>[
      //   TextButton(
      //     onPressed: () {
      //       Navigator.of(context).pop();
      //     },
      //     child: const Text('CANCEL'),
      //   ),
      //   TextButton(
      //     onPressed: () {
      //       Navigator.of(context).pop();
      //     },
      //     child: const Text('ACCEPT'),
      //   ),
      // ],
    );
  }

  Card buildCardSelectService() {
    return Card(
      margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
      shadowColor: Colors.black87,
      elevation: 8,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
      color: Colors.white,
      child: Column(
        children: [
          buildCardWalterNeedCook("Need Cook", "Booked"),
          Container(
            // height: 100,
            child: Row(
              children: [
                Container(
                  // alignment: Alignment.topLeft,
                  // color: Colors.blue,
                  width: 381,
                  child: Row(
                    children: [
                      Image(
                        image: AssetImage('assets/images/img_track_now.png'),
                        // fit: BoxFit.fill,
                      ),
                      Column(
                        children: [
                          Container(
                            width: 230,
                            // color: Colors.amber,
                            // margin: EdgeInsets.fromLTRB(0, 17.0, 19.0, 0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                  child: Text(
                                    "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit",
                                    maxLines: 3,
                                    overflow: TextOverflow.ellipsis,
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Color(0xff000000),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(padding: EdgeInsets.only(top: 15)),
                          Container(
                            width: 230,
                            // color: Colors.amber,
                            // margin: EdgeInsets.fromLTRB(19.0, 17.0, 19.0, 0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(right: 9),
                                  child: SvgPicture.asset(iconTv),
                                ),
                                Text(
                                  "Before the 08 Nov 2018.",
                                  style: GoogleFonts.poppins(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500,
                                    color: Color(0xff000000),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 230,
                            // color: Colors.amber,
                            // margin: EdgeInsets.fromLTRB(19.0, 17.0, 19.0, 0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(children: [
                                  Padding(
                                    padding: EdgeInsets.only(right: 9),
                                    child: SvgPicture.asset(iconPlace),
                                  ),
                                  Text(
                                    "3 ml",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500,
                                      color: Color(0xff000000),
                                    ),
                                  )
                                ]),
                                SizedBox(height: 32),
                                Row(children: [
                                  Text(
                                    "Budget:",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w600,
                                      color: Color(0xff000000),
                                    ),
                                  ),
                                  Text(
                                    "\$240",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w600,
                                      color: Color(0xff33B440),
                                    ),
                                  ),
                                ]),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Card buildCardWalterNeedCook(String text1, String text2) {
    return Card(
      margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
      shadowColor: Colors.black87,
      elevation: 0,
      // color: Colors.green,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
      child: Container(
        // color: Colors.amber,
        child: Row(
          // crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: Row(
                children: [
                  Stack(
                    children: [
                      Positioned(
                        child: Container(
                          width: 150,
                          alignment: Alignment.topLeft,
                          // color: Colors.red,
                          margin: EdgeInsets.fromLTRB(15, 0, 0, 0),
                          // padding: EdgeInsets.all(0),
                          child: (Column(
                              // mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: 10),
                                Text(
                                  text1,
                                  style: GoogleFonts.poppins(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500,
                                    color: Color(0xff000000),
                                  ),
                                ),
                                Text(
                                  text2,
                                  style: GoogleFonts.poppins(
                                    fontSize: 10,
                                    fontWeight: FontWeight.w600,
                                    color: Color(0xffFF0000),
                                  ),
                                ),
                              ])),
                        ),
                      ),
                      Positioned(
                          left: 95,
                          top: 7,
                          child: Padding(
                            padding: EdgeInsets.only(left: 8),
                            child: SvgPicture.asset(iconCook),
                          )),
                    ],
                  ),
                ],
              ),
            ),
            Container(
                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                child: PopupMenuButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(6.0))),
                    itemBuilder: (_) => <PopupMenuItem<String>>[
                          new PopupMenuItem<String>(
                              padding: EdgeInsets.all(0),
                              child: Container(
                                width: 112,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          10, 4, 0, 4),
                                      child: Text(
                                        "Edit",
                                        style: GoogleFonts.poppins(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xff000000),
                                        ),
                                      ),
                                    ),
                                    Divider(
                                      color: Color(0xff212121),
                                    ),
                                    Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            0, 2, 0, 2),
                                        child: Container(
                                          height: 30,
                                          // color: Colors.amber,
                                          child: ElevatedButton(
                                            onPressed: () {
                                              showDialog(
                                                context: context,
                                                builder: (BuildContext
                                                        context) =>
                                                    _buildPopupDialog(context),
                                              );
                                            },
                                            child: Text(
                                              "Cancel The Job",
                                              style: GoogleFonts.poppins(
                                                fontSize: 12,
                                                fontWeight: FontWeight.w400,
                                                color: Color(0xff212121),
                                              ),
                                            ),
                                            style: ElevatedButton.styleFrom(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      10, 4, 0, 4),
                                              primary: Colors.transparent,
                                              // onPrimary: Colors.black,
                                              shadowColor: Colors.transparent,
                                            ),
                                          ),
                                        )),
                                    Divider(
                                      color: Colors.black,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          10, 4, 0, 4),
                                      child: Text(
                                        "Delete",
                                        style: GoogleFonts.poppins(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xff000000),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              value: 'report'),
                        ],
                    onSelected: (index) async {
                      switch (index) {
                        case 'report':
                          // showDialog(
                          //     barrierDismissible: true,
                          //     context: context,
                          //     builder: (context) => ReportUser(
                          //       currentUser: widget.sender,
                          //       seconduser: widget.second,
                          //     )).then((value) => Navigator.pop(ct))
                          break;
                      }
                    })),
          ],
        ),
        // Container(
        //   color: Colors.blue,
        //   margin: EdgeInsets.fromLTRB(19, 0, 0, 0),
        //   child: Row(
        //     children: [
        //       Text("Applied By 18"),
        //     ],
        //   ),
        // ),
      ),
    );
  }

  Widget buildSwitchServiceWalter() => Transform.scale(
        scale: 0.6,
        child: CupertinoSwitch(
          activeColor: Color(0xff5068A8),
          value: valServiceWalter,
          onChanged: (valServiceWalter) =>
              setState(() => this.valServiceWalter = valServiceWalter),
        ),
      );
}
