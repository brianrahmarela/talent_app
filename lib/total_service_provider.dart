import 'dart:ui';

import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:talent_app/styles/icon.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'total_service_provider2.dart';

class TotalServiceProvider extends StatefulWidget {
  const TotalServiceProvider({Key? key}) : super(key: key);

  @override
  _TotalServiceProviderState createState() => _TotalServiceProviderState();
}

class _TotalServiceProviderState extends State<TotalServiceProvider> {
  bool valServiceWalter = true;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          extendBodyBehindAppBar: true,
          backgroundColor: Colors.white,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: new IconButton(
              icon: new Icon(Icons.arrow_back_ios),
              onPressed: () => Navigator.of(context).pop(),
            ),
            title: Text(
              "Total Service Providers (18)",
              style: GoogleFonts.poppins(
                  fontSize: 18, fontWeight: FontWeight.w600),
            ),
            bottom: TabBar(
              labelStyle: GoogleFonts.poppins(
                  fontSize: 16, fontWeight: FontWeight.w400), //For Selected tab

              unselectedLabelColor: Colors.white54,
              // indicatorColor: Color(0xffffffff),
              indicator: UnderlineTabIndicator(
                  // borderSide: BorderSide(width: 2.0),
                  insets: EdgeInsets.symmetric(horizontal: 50.0),
                  borderSide: BorderSide(color: Colors.white, width: 3)),
              indicatorSize: TabBarIndicatorSize.label,
              // isScrollable: true,
              tabs: [
                Tab(
                  text: "List View",
                ),
                Tab(text: "Map View"),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              Container(
                height: 760,
                // padding: EdgeInsets.only(bottom: 50),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/images/bg_myjob.png'),
                    fit: BoxFit.fill,
                  ),
                ),
                child: SafeArea(
                    child: ListView(
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 46, 0, 15),
                      child: buildCardTop(),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 15),
                      child: buildCardTop(),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 15),
                      child: buildCardTop(),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 15),
                      child: buildCardTop(),
                    ),
                    SizedBox(height: 55),
                  ],
                )),
              ),
              TotalServiceProvider2(),
            ],
          ),
        ));
  }

  Card buildCardTop() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
        shadowColor: Colors.black87,
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            // color: Colors.amberAccent,
            margin: EdgeInsets.fromLTRB(19.0, 17.0, 19.0, 0),
            // width: 330,
            child: Row(
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  // color: Colors.black,
                  // height: 70,
                  child: Stack(
                    children: [
                      Positioned(
                          child: Card(
                        elevation: 3.5,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(60.0),
                        ),
                        color: Color(0x212121),
                        child: Container(
                          alignment: Alignment.centerLeft,
                          height: 59,
                          width: 59,
                          child: CircleAvatar(
                            backgroundImage:
                                AssetImage('assets/images/avatar_rating.png'),
                            radius: 60,
                          ),
                        ),
                      )),
                      Positioned(
                          left: 0,
                          top: 38,
                          child: SvgPicture.asset(iconOutlineGreen))
                    ],
                  ),
                ),
                Container(
                  // color: Color(0xffaeaeae),
                  margin: EdgeInsets.only(left: 8),
                  width: 200,
                  // color: Colors.red,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            "Clayton L.",
                            style: GoogleFonts.poppins(
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              color: Color(0xff000000),
                            ),
                          ),
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(top: 4.7)),
                      Row(
                        // mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Icon(
                            Icons.star_rate,
                            color: Colors.yellow.shade700,
                            size: 14.0,
                          ),
                          Text(
                            " 5",
                            style: GoogleFonts.poppins(
                              fontSize: 11,
                              fontWeight: FontWeight.w600,
                              // color: Color(0xff33B440),
                            ),
                          ),
                        ],
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 7.9),
                        child: Column(
                          // mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Padding(
                                  padding: EdgeInsets.fromLTRB(0, 0, 9, 0),
                                  child: SvgPicture.asset(iconTv),
                                ),
                                Text(
                                  "Appt. On 8 Nov 2018",
                                  style: GoogleFonts.poppins(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xff212121),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [],
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(94.0, 6.0, 19.0, 0),
            child: Row(
              children: [
                Text(
                  "Plumber & 5 More",
                  style: GoogleFonts.poppins(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff949494),
                  ),
                ),
              ],
            ),
          ),
          Container(
            // color: Color,
            height: 39,
            margin: EdgeInsets.only(top: 14),
            padding: EdgeInsets.zero,
            child: Row(
              children: [
                Flexible(
                  flex: 1,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    style: ElevatedButton.styleFrom(
                        padding: EdgeInsets.zero,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20))),
                    child: Ink(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              colors: [Color(0xffE4E4E4), Color(0xffB2B2B2)]),
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(10))),
                      child: Container(
                        // width: 378,
                        margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        // height: 47,
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'REJECT',
                              overflow: TextOverflow.ellipsis,
                              style: GoogleFonts.poppins(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                color: Color(0xff212121),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Flexible(
                  flex: 2,

                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    style: ElevatedButton.styleFrom(
                        padding: EdgeInsets.zero,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20))),
                    child: Ink(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              colors: [Color(0xff5068A8), Color(0xff5CB5CF)]),
                          borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(10))),
                      child: Container(
                        // width: 378,
                        // margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
                        padding: EdgeInsets.zero,
                        // height: 47,
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'ACCEPT',
                              overflow: TextOverflow.ellipsis,
                              style: GoogleFonts.poppins(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                color: Color(0xffffffff),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  // ),
                ),
              ],
            ),
          )
        ]));
  }
}
