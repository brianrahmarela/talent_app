import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:talent_app/styles/icon.dart';
import 'package:flutter_svg/flutter_svg.dart';

class RatingReview2 extends StatefulWidget {
  const RatingReview2({Key? key}) : super(key: key);

  @override
  _RatingReview2State createState() => _RatingReview2State();
}

class _RatingReview2State extends State<RatingReview2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          "Rating & Review",
          style: GoogleFonts.poppins(fontSize: 18, fontWeight: FontWeight.w600),
        ),
      ),
      body: Container(
        height: 760,
        // padding: EdgeInsets.only(bottom: 50),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/bg_appbar.png'),
            fit: BoxFit.fill,
          ),
        ),
        child: SafeArea(
            child: ListView(
          children: [
            Stack(
              children: [
                Positioned(
                    child: Container(
                  // height: 140,
                  // width: 140,
                  // color: Colors.deepOrange,
                  alignment: Alignment.topCenter,
                  margin: EdgeInsets.only(top: 75),
                  child: CircleAvatar(
                    backgroundImage:
                        AssetImage('assets/images/avatar_profile.png'),
                    radius: 60,
                  ),
                )),
                Positioned(
                    top: 79,
                    left: 220,
                    child: Container(
                      // decoration: BoxDecoration(
                      //     borderRadius: BorderRadius.circular(30.0)),
                      alignment: Alignment.topCenter,
                      margin: EdgeInsets.only(top: 75),
                      child: CircleAvatar(
                        backgroundColor: Color(0xff5068A8),
                        child: SvgPicture.asset(iconCamera),
                      ),
                    )),
              ],
            ),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.only(top: 15),
              child: Text(
                "John Doe",
                style: GoogleFonts.poppins(
                    fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              // alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset(iconrating_4_5),
                  Container(
                    margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: SvgPicture.asset(iconLineVertical),
                  ),
                  Text("3 Reviews",
                      style: GoogleFonts.poppins(
                          fontSize: 12,
                          fontWeight: FontWeight.w500,
                          color: Color(0xff5068A8))),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 26.6, 0, 15.0),
              child: buildCardTop(),
              width: 530,
              height: 294,
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 26.6, 0, 15.0),
              // padding: EdgeInsets.only(bottom: 30),
              child: buildCardTop(),
              width: 530,
              height: 294,
            ),
            SizedBox(height: 30)
          ],
        )),
      ),
    );
  }

  Card buildCardTop() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
        shadowColor: Colors.black87,
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            // color: Colors.amberAccent,
            margin: EdgeInsets.fromLTRB(19.0, 17.0, 19.0, 0),
            // width: 330,
            child: Row(
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  // color: Colors.black,
                  // height: 70,
                  child: Stack(
                    children: [
                      Positioned(
                          child: Container(
                        height: 62,
                        width: 59,
                        alignment: Alignment.centerLeft,
                        // margin: EdgeInsets.only(top: 15),
                        child: CircleAvatar(
                          backgroundImage:
                              AssetImage('assets/images/avatar_rating.png'),
                          radius: 60,
                        ),
                      )),
                      Positioned(
                          left: 0,
                          top: 36,
                          child: SvgPicture.asset(iconCheckGreen))
                    ],
                  ),
                ),
                Container(
                  // color: Color(0xffffffff),
                  margin: EdgeInsets.only(left: 8),
                  width: 200,
                  // color: Colors.red,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            "Clayton L.",
                            style: GoogleFonts.poppins(
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              color: Color(0xff000000),
                            ),
                          ),
                          Container(
                            height: 14,
                            child: SvgPicture.asset(iconAvatarSubscription),
                          ),
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(top: 5.8)),
                      Row(
                        // mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SvgPicture.asset(iconrating_4_5),
                        ],
                      ),
                      Row(
                        // mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            width: 220,
                            margin: EdgeInsets.fromLTRB(0, 9, 0, 0),
                            padding: EdgeInsets.fromLTRB(12, 2, 12, 2),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                color: Color(0xffF0BB3F),
                                borderRadius: BorderRadius.circular(8.0)),
                            constraints:
                                BoxConstraints(maxWidth: 135, minHeight: 15),
                            child: Text.rich(
                              TextSpan(
                                text: "Plumber & 5 More",
                                style: GoogleFonts.poppins(
                                  fontSize: 9,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xffffffff),
                                ),
                              ),
                            ),
                          )

                          // ],
                          // ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(19.0, 19.0, 19.0, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Vestibulum",
                  style: GoogleFonts.poppins(
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                    color: Color(0xff000000),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(19, 9, 19, 2),
            child: Column(
              children: [
                Text(
                  "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit....  Stet clita..",
                  style: GoogleFonts.poppins(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff949494),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(19, 2, 0, 0),
            alignment: Alignment.centerLeft,
            child: Text(
              "07/08/2018",
              style: GoogleFonts.poppins(
                fontSize: 8,
                fontWeight: FontWeight.w400,
                color: Color(0xff949494),
              ),
            ),
          )
        ]));
  }
}
