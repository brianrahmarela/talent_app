import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ChooseLanguage extends StatefulWidget {
  const ChooseLanguage({Key? key}) : super(key: key);

  @override
  _ChooseLanguageState createState() => _ChooseLanguageState();
}

class _ChooseLanguageState extends State<ChooseLanguage> {
  bool valueArabic = false;
  bool valueEspanol = false;
  bool valueChina = false;
  bool valueEnglish = false;
  bool valuePortugis = false;
  bool valueThailand = false;
  bool valuePrancis = false;
  bool valueItalia = false;
  // List<Widget> widgets = [];
  // int counter = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          "Choose Language",
          style: GoogleFonts.poppins(fontSize: 18, fontWeight: FontWeight.w600),
        ),
      ),
      body: Container(
          // padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/bg_appbar.png'),
              fit: BoxFit.fill,
            ),
          ),
          child: SafeArea(
            child: ListView(padding: EdgeInsets.only(top: 69), children: [
              CheckboxListTile(
                // controlAffinity: ListTileControlAffinity.leading,
                value: valueArabic,
                onChanged: (valueArabic) =>
                    setState(() => this.valueArabic = valueArabic!),
                title: Text(
                  'اللغة العربية',
                  style: GoogleFonts.poppins(
                      fontSize: 14, fontWeight: FontWeight.w500),
                ),
                activeColor: Colors.transparent,
                checkColor: Color(0xff4F78E4),
              ),
              Divider(
                color: Colors.grey,
                // height: 17,
              ),
              CheckboxListTile(
                // controlAffinity: ListTileControlAffinity.leading,
                value: valueEspanol,
                onChanged: (valueEspanol) =>
                    setState(() => this.valueEspanol = valueEspanol!),
                title: Text(
                  'español',
                  style: GoogleFonts.poppins(
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                activeColor: Colors.transparent,
                checkColor: Color(0xff4F78E4),
                // selectedTileColor: Color(0xff4F78E4),
              ),
              Divider(
                color: Colors.grey,
                // height: 17,
              ),
              CheckboxListTile(
                // controlAffinity: ListTileControlAffinity.leading,
                value: valueChina,
                onChanged: (valueChina) =>
                    setState(() => this.valueChina = valueChina!),
                title: Text(
                  '中国',
                  style: GoogleFonts.poppins(
                      fontSize: 14, fontWeight: FontWeight.w500),
                ),
                activeColor: Colors.transparent,
                checkColor: Color(0xff4F78E4),
              ),
              Divider(
                color: Colors.grey,
                // height: 17,
              ),
              CheckboxListTile(
                // controlAffinity: ListTileControlAffinity.leading,
                value: valueEnglish,
                onChanged: (valueEnglish) =>
                    setState(() => this.valueEnglish = valueEnglish!),
                title: Text(
                  'English',
                  style: GoogleFonts.poppins(
                      fontSize: 14, fontWeight: FontWeight.w500),
                ),
                activeColor: Colors.transparent,
                checkColor: Color(0xff4F78E4),
              ),
              Divider(
                color: Colors.grey,
                // height: 17,
              ),
              CheckboxListTile(
                // controlAffinity: ListTileControlAffinity.leading,
                value: valueEnglish,
                onChanged: (valuePortugis) =>
                    setState(() => this.valuePortugis = valuePortugis!),
                title: Text(
                  'português',
                  style: GoogleFonts.poppins(
                      fontSize: 14, fontWeight: FontWeight.w500),
                ),
                activeColor: Colors.transparent,
                checkColor: Color(0xff4F78E4),
              ),
              Divider(
                color: Colors.grey,
                // height: 17,
              ),
              CheckboxListTile(
                // controlAffinity: ListTileControlAffinity.leading,
                value: valueThailand,
                onChanged: (valueThailand) =>
                    setState(() => this.valueThailand = valueThailand!),
                title: Text(
                  'இந்தியா',
                  style: GoogleFonts.poppins(
                      fontSize: 14, fontWeight: FontWeight.w500),
                ),
                activeColor: Colors.transparent,
                checkColor: Color(0xff4F78E4),
              ),
              Divider(
                color: Colors.grey,
                // height: 17,
              ),
              CheckboxListTile(
                // controlAffinity: ListTileControlAffinity.leading,
                value: valuePrancis,
                onChanged: (valuePrancis) =>
                    setState(() => this.valuePrancis = valuePrancis!),
                title: Text(
                  'français',
                  style: GoogleFonts.poppins(
                      fontSize: 14, fontWeight: FontWeight.w500),
                ),
                activeColor: Colors.transparent,
                checkColor: Color(0xff4F78E4),
              ),
              Divider(
                color: Colors.grey,
                // height: 17,
              ),
              CheckboxListTile(
                // controlAffinity: ListTileControlAffinity.leading,
                value: valueItalia,
                onChanged: (valueItalia) =>
                    setState(() => this.valueItalia = valueItalia!),
                title: Text(
                  'Italiano',
                  style: GoogleFonts.poppins(
                      fontSize: 14, fontWeight: FontWeight.w500),
                ),
                activeColor: Colors.transparent,
                checkColor: Color(0xff4F78E4),
              ),
              Divider(
                color: Colors.grey,
                // height: 17,
              ),
              SizedBox(
                height: 259,
              ),
            ]),
          )

          //
          ),
    );
  }
}
