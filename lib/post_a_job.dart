import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:talent_app/styles/icon.dart';
import 'package:flutter_svg/flutter_svg.dart';

class PostAJob extends StatefulWidget {
  const PostAJob({Key? key}) : super(key: key);

  @override
  _PostAJobState createState() => _PostAJobState();
}

class _PostAJobState extends State<PostAJob> {
  bool val1 = true;
  bool valServiceProvider = true;
  bool valServiceWalter = true;
  bool valServicePainting = true;
  bool valServiceDogWalking = true;

  onChangeFunction1(bool newValue1) {
    val1 = newValue1;
  }

  @override
  Widget build(BuildContext context) {
    // PageController controller =
    //     PageController(initialPage: 0, viewportFraction: 0.8);
    // List<Color> colors = [Colors.red, Colors.green];

    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          "Post a job",
          style: GoogleFonts.poppins(fontSize: 18, fontWeight: FontWeight.w600),
        ),
      ),
      body: Container(
        // height: 760,
        // padding: EdgeInsets.only(bottom: 50),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/bg_appbar.png'),
            fit: BoxFit.fill,
          ),
        ),
        child: SafeArea(
            child: SizedBox(
          child: ListView(
            children: [
              buildCardCategory(),
              buildCardEnterJobAddress(),
              buildCardJobBAsicDetail(),
              buildCardSetDates(),
              // SizedBox(height: 15),
              buildCardSLider(),
              // SizedBox(height: 15),
              // SizedBox(height: 15),
              buildCardLinkedin(),

              Container(
                // alignment: Alignment.center,
                child: Row(
                  children: [
                    Container(
                      width: 171,
                      // height: 20,
                      margin: EdgeInsets.fromLTRB(17, 32.0, 17, 54),
                      child: SizedBox(
                        // width: 156,
                        child: ElevatedButton(
                          onPressed: () {
                            showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                                  _buildPopupDialog(context),
                            );
                          },
                          style: ElevatedButton.styleFrom(
                              // minimumSize: Size(156, 43),
                              // fixedSize: Size(156, 43),
                              padding: EdgeInsets.zero,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30))),
                          child: Ink(
                            decoration: BoxDecoration(
                                gradient: LinearGradient(colors: [
                                  Color(0xffE4E4E4),
                                  Color(0xffB2B2B2)
                                ]),
                                borderRadius: BorderRadius.circular(30)),
                            child: Container(
                              // width: 156,
                              // margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
                              height: 43,
                              alignment: Alignment.center,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    'CANCEL',
                                    overflow: TextOverflow.ellipsis,
                                    style: GoogleFonts.poppins(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w600,
                                      color: Color(0xff000000),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width: 171,
                      // height: 20,
                      margin: EdgeInsets.fromLTRB(17, 32.0, 17, 54),
                      child: SizedBox(
                        // width: 156,
                        child: ElevatedButton(
                          onPressed: () {
                            // print('Hi there');
                          },
                          style: ElevatedButton.styleFrom(
                              // minimumSize: Size(156, 43),
                              // fixedSize: Size(156, 43),
                              padding: EdgeInsets.zero,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20))),
                          child: Ink(
                            decoration: BoxDecoration(
                                gradient: LinearGradient(colors: [
                                  Color(0xff5068A8),
                                  Color(0xff5CB5CF)
                                ]),
                                borderRadius: BorderRadius.circular(30)),
                            child: Container(
                              // width: 156,
                              // margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
                              height: 43,
                              alignment: Alignment.center,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    'POST A JOB',
                                    overflow: TextOverflow.ellipsis,
                                    style: GoogleFonts.poppins(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w600,
                                      color: Color(0xffffffff),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        )),
      ),
    );
  }

  Widget _buildPopupDialog(BuildContext context) {
    return AlertDialog(
      insetPadding: EdgeInsets.all(15),
      contentPadding: EdgeInsets.all(0),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(6))),
      // backgroundColor: Colors.amber,
      // title: const Text('Enter Current Password'),
      // title: Divider(
      //   color: Colors.black,
      //   // height: 17,
      // ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            margin: EdgeInsets.fromLTRB(0, 15, 0, 13.6),
            child: Text("Reason for Cancelling",
                style: GoogleFonts.poppins(
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                )),
          ),
          Divider(
            color: Colors.black,
            // height: 17,
          ),
          Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.fromLTRB(29, 15, 29, 11),
            child: Text("Write your reason below",
                textAlign: TextAlign.center,
                style: GoogleFonts.poppins(
                  fontSize: 12,
                  fontWeight: FontWeight.w600,
                  color: Color(0xff949494),
                )),
          ),
          Container(
              // width: 269.34,
              // height: 59,
              margin: EdgeInsets.fromLTRB(29, 0, 29, 29.4),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextField(
                    decoration: InputDecoration(
                      hintText: 'Type here',
                      // labelText: 'Type here',
                      labelStyle: GoogleFonts.poppins(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: Color(0xffB2B2B2),
                      ),
                    ),
                    keyboardType: TextInputType.visiblePassword,
                    // textInputAction: TextInputAction.done,
                    obscureText: true,
                  ),
                ],
              )),
          Row(
            // mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                decoration: BoxDecoration(
                    // color: Colors.grey,
                    borderRadius: BorderRadius.only(
                  bottomLeft: const Radius.circular(6.0),
                )),
                width: 181,
                margin: EdgeInsets.only(top: 25),
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  style: ElevatedButton.styleFrom(
                      padding: EdgeInsets.zero,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20))),
                  child: Ink(
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            colors: [Color(0xffE4E4E4), Color(0xffB2B2B2)]),
                        borderRadius:
                            BorderRadius.only(bottomLeft: Radius.circular(6))),
                    child: Container(
                      width: 378,
                      // margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
                      height: 51,
                      alignment: Alignment.center,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'CANCEL',
                            overflow: TextOverflow.ellipsis,
                            style: GoogleFonts.poppins(
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              color: Color(0xff212121),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                // alignment: Alignment.center,
                width: 181,
                margin: EdgeInsets.only(top: 25),
                // color: Colors.red,
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  style: ElevatedButton.styleFrom(
                      padding: EdgeInsets.zero,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20))),
                  child: Ink(
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            colors: [Color(0xff5068A8), Color(0xff5CB5CF)]),
                        borderRadius:
                            BorderRadius.only(bottomRight: Radius.circular(6))),
                    child: Container(
                      width: 378,
                      // margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
                      height: 51,
                      alignment: Alignment.center,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'VERIFY',
                            overflow: TextOverflow.ellipsis,
                            style: GoogleFonts.poppins(
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              color: Color(0xffffffff),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
      // actions: <Widget>[
      //   TextButton(
      //     onPressed: () {
      //       Navigator.of(context).pop();
      //     },
      //     child: const Text('CANCEL'),
      //   ),
      //   TextButton(
      //     onPressed: () {
      //       Navigator.of(context).pop();
      //     },
      //     child: const Text('ACCEPT'),
      //   ),
      // ],
    );
  }

  Card buildCardCategory() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 46, 15.0, 15),
        shadowColor: Colors.black87,
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.fromLTRB(19, 15, 0, 17),
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Select a Category",
                        style: GoogleFonts.poppins(
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                          color: Color(0xff000000),
                        )),
                    Text("Home Exterior",
                        style: GoogleFonts.poppins(
                          fontSize: 10.5,
                          fontWeight: FontWeight.w400,
                          color: Color(0xff949494),
                        )),
                  ],
                )),
              ],
            ),
          ),
        ]));
  }

  Card buildCardEnterJobAddress() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 15),
        shadowColor: Colors.black87,
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.fromLTRB(19, 15, 19, 16.2),
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  // color: Colors.amber,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Enter Job Address",
                          style: GoogleFonts.poppins(
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                            color: Color(0xff000000),
                          )),
                      Text(
                          "It will show only once the job is Booked or \nscheduled",
                          style: GoogleFonts.poppins(
                            fontSize: 10.5,
                            fontWeight: FontWeight.w400,
                            color: Color(0xff949494),
                          )),

                      // alignment: Alignment.topCenter,
                      // margin: EdgeInsets.fromLTRB(12.0, 0, 12.0, 0),

                      Column(
                        children: [
                          SizedBox(
                            height: 17.0,
                          ),
                          TextFormField(
                            style: GoogleFonts.poppins(
                                fontSize: 12, fontWeight: FontWeight.w500),
                            decoration: InputDecoration(
                              labelText: "Street Address",
                            ),
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          // SizedBox(
                          //   height: 10.0,
                          // ),
                          Row(
                            children: <Widget>[
                              Flexible(
                                child: TextFormField(
                                  style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500),
                                  decoration:
                                      InputDecoration(labelText: "City"),
                                ),
                              ),
                              Padding(padding: EdgeInsets.only(left: 20)),
                              Flexible(
                                child: TextFormField(
                                  style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500),
                                  decoration:
                                      InputDecoration(labelText: "State"),
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          Row(
                            children: <Widget>[
                              Flexible(
                                child: TextFormField(
                                  style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500),
                                  decoration:
                                      InputDecoration(labelText: "Zip Code"),
                                ),
                              ),
                              Padding(padding: EdgeInsets.only(left: 20)),
                              Flexible(
                                child: TextFormField(
                                  style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500),
                                  decoration:
                                      InputDecoration(labelText: "Country"),
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                      // ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ]));
  }

  Card buildCardJobBAsicDetail() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 15),
        shadowColor: Colors.black87,
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.fromLTRB(19, 15, 19, 17),
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  // color: Colors.amber,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Enter Job Basic Details",
                          style: GoogleFonts.poppins(
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                            color: Color(0xff000000),
                          )),

                      Column(
                        children: [
                          SizedBox(
                            height: 10.0,
                          ),
                          TextFormField(
                            style: GoogleFonts.poppins(
                                fontSize: 12, fontWeight: FontWeight.w500),
                            decoration: InputDecoration(
                              labelText: "Job Title",
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          SizedBox(
                            height: 20.0,
                          ),
                          TextFormField(
                            style: GoogleFonts.poppins(
                                fontSize: 12, fontWeight: FontWeight.w500),
                            decoration: InputDecoration(
                              labelText: "Job Description",
                            ),
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          TextFormField(
                            style: GoogleFonts.poppins(
                                fontSize: 12, fontWeight: FontWeight.w500),
                            decoration: InputDecoration(
                              labelText: "Budger",
                            ),
                          ),
                          SizedBox(
                            height: 21.2,
                          ),
                        ],
                      ),
                      // ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ]));
  }

  Card buildCardSetDates() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 15),
        shadowColor: Colors.black87,
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.fromLTRB(19, 15, 19, 17),
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  // color: Colors.amber,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Set Dates",
                          style: GoogleFonts.poppins(
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                            color: Color(0xff000000),
                          )),

                      Column(
                        children: [
                          SizedBox(
                            height: 10.0,
                          ),
                          TextFormField(
                            style: GoogleFonts.poppins(
                                fontSize: 12, fontWeight: FontWeight.w500),
                            decoration: InputDecoration(
                              labelText: "Need to be done before",
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          SizedBox(
                            height: 20.0,
                          ),
                          TextFormField(
                            style: GoogleFonts.poppins(
                                fontSize: 12, fontWeight: FontWeight.w500),
                            decoration: InputDecoration(
                              labelText: "Expiry Date & Time",
                            ),
                          ),
                          SizedBox(
                            height: 21.2,
                          ),
                        ],
                      ),

                      // ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ]));
  }

  Widget buildImg(String text) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              Positioned(
                child: Container(
                  width: 99,
                  height: 99,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/images/img_track_now.png')),
                  ),
                ),
              ),
              Positioned(
                top: 5,
                left: 72,
                child: CircleAvatar(
                  radius: 10,
                  backgroundColor: Color(0xff5068A8),
                ),
              ),
              Positioned(
                left: 66.5,
                child: SizedBox(
                  height: 14,
                  width: 14,
                  child: IconButton(
                    color: Color(0xffffffff),
                    onPressed: () {},
                    icon: Icon(
                      Icons.close,
                      size: 14.0,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(left: 13),
            child: Text(
              text,
              // textAlign: TextAlign.left,
              style: GoogleFonts.poppins(
                fontSize: 11,
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
        ],
      ),
      // child:
    );
  }

  Widget buildImgVid1(String text) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              Positioned(
                child: Container(
                  width: 99,
                  height: 99,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/images/video-1.png')),
                  ),
                ),
              ),
              Positioned(
                top: 5,
                left: 72,
                child: CircleAvatar(
                  radius: 10,
                  backgroundColor: Color(0xff5068A8),
                ),
              ),
              Positioned(
                left: 66.5,
                child: SizedBox(
                  height: 14,
                  width: 14,
                  child: IconButton(
                    color: Color(0xffffffff),
                    onPressed: () {},
                    icon: Icon(
                      Icons.close,
                      size: 14.0,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(left: 13),
            child: Text(
              text,
              // textAlign: TextAlign.left,
              style: GoogleFonts.poppins(
                fontSize: 11,
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
        ],
      ),
      // child:
    );
  }

  Widget buildImgVid2(String text) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              Positioned(
                child: Container(
                  width: 99,
                  height: 99,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/images/video-2.png')),
                  ),
                ),
              ),
              Positioned(
                top: 5,
                left: 72,
                child: CircleAvatar(
                  radius: 10,
                  backgroundColor: Color(0xff5068A8),
                ),
              ),
              Positioned(
                left: 66.5,
                child: SizedBox(
                  height: 14,
                  width: 14,
                  child: IconButton(
                    color: Color(0xffffffff),
                    onPressed: () {},
                    icon: Icon(
                      Icons.close,
                      size: 14.0,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(left: 13),
            child: Text(
              text,
              // textAlign: TextAlign.left,
              style: GoogleFonts.poppins(
                fontSize: 11,
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
        ],
      ),
      // child:
    );
  }

  Widget buildImgWeb1(String text) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              Positioned(
                child: Container(
                  width: 99,
                  height: 99,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/images/web-1.png')),
                  ),
                ),
              ),
              Positioned(
                top: 5,
                left: 72,
                child: CircleAvatar(
                  radius: 10,
                  backgroundColor: Color(0xff5068A8),
                ),
              ),
              Positioned(
                left: 66.5,
                child: SizedBox(
                  height: 14,
                  width: 14,
                  child: IconButton(
                    color: Color(0xffffffff),
                    onPressed: () {},
                    icon: Icon(
                      Icons.close,
                      size: 14.0,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(left: 13),
            child: Text(
              text,
              // textAlign: TextAlign.left,
              style: GoogleFonts.poppins(
                fontSize: 11,
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
        ],
      ),
      // child:
    );
  }

  Widget buildImgLinkedin(String text) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              Positioned(
                child: Container(
                  width: 99,
                  height: 99,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/images/linkedinfoto.png')),
                  ),
                ),
              ),
              Positioned(
                top: 5,
                left: 72,
                child: CircleAvatar(
                  radius: 10,
                  backgroundColor: Color(0xff5068A8),
                ),
              ),
              Positioned(
                left: 66.5,
                child: SizedBox(
                  height: 14,
                  width: 14,
                  child: IconButton(
                    color: Color(0xffffffff),
                    onPressed: () {},
                    icon: Icon(
                      Icons.close,
                      size: 14.0,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(left: 13),
            child: Text(
              text,
              // textAlign: TextAlign.left,
              style: GoogleFonts.poppins(
                fontSize: 11,
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
        ],
      ),
      // child:
    );
  }

  Widget buildSwitchProvider() => Transform.scale(
        scale: 0.9,
        child: CupertinoSwitch(
          activeColor: Color(0xff5068A8),
          value: valServiceProvider,
          onChanged: (valServiceProvider) =>
              setState(() => this.valServiceProvider = valServiceProvider),
        ),
      );

  Widget buildSwitchServiceWalter() => Transform.scale(
        scale: 0.6,
        child: CupertinoSwitch(
          activeColor: Color(0xff5068A8),
          value: valServiceWalter,
          onChanged: (valServiceWalter) =>
              setState(() => this.valServiceWalter = valServiceWalter),
        ),
      );

  Widget buildSwitchServicePainting() => Transform.scale(
        scale: 0.6,
        child: CupertinoSwitch(
          activeColor: Color(0xff5068A8),
          value: valServicePainting,
          onChanged: (valServicePainting) =>
              setState(() => this.valServicePainting = valServicePainting),
        ),
      );

  Widget buildSwitchServiceDogWalking() => Transform.scale(
        scale: 0.6,
        child: CupertinoSwitch(
          activeColor: Color(0xff5068A8),
          value: valServiceDogWalking,
          onChanged: (valServiceDogWalking) =>
              setState(() => this.valServiceDogWalking = valServiceDogWalking),
        ),
      );

  Card buildCardTop() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
        shadowColor: Colors.black87,
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            // color: Colors.amberAccent,
            margin: EdgeInsets.fromLTRB(19.0, 17.0, 19.0, 0),
            // width: 330,
            child: Row(
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  // color: Colors.black,
                  // height: 70,
                  child: Stack(
                    children: [
                      Positioned(
                          child: Container(
                        height: 62,
                        width: 59,
                        alignment: Alignment.centerLeft,
                        // margin: EdgeInsets.only(top: 15),
                        child: CircleAvatar(
                          backgroundImage:
                              AssetImage('assets/images/avatar_rating.png'),
                          radius: 60,
                        ),
                      )),
                      Positioned(
                          left: 0,
                          top: 36,
                          child: SvgPicture.asset(iconCheckGreen))
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 8),
                  width: 200,
                  // color: Colors.red,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            "Clayton L.",
                            style: GoogleFonts.poppins(
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              color: Color(0xff000000),
                            ),
                          ),
                          Container(
                            height: 14,
                            child: SvgPicture.asset(iconAvatarSubscription),
                          ),
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(top: 5.8)),
                      Row(
                        // mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SvgPicture.asset(iconrating_4_5),
                        ],
                      ),
                      Row(
                        // mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            width: 220,
                            margin: EdgeInsets.fromLTRB(0, 9, 0, 0),
                            padding: EdgeInsets.fromLTRB(12, 2, 12, 2),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                color: Color(0xffF0BB3F),
                                borderRadius: BorderRadius.circular(8.0)),
                            constraints:
                                BoxConstraints(maxWidth: 135, minHeight: 15),
                            child: Text.rich(
                              TextSpan(
                                text: "Plumber & 5 More",
                                style: GoogleFonts.poppins(
                                  fontSize: 9,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xffffffff),
                                ),
                              ),
                            ),
                          )

                          // ],
                          // ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(19.0, 19.0, 19.0, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Vestibulum",
                  style: GoogleFonts.poppins(
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                    color: Color(0xff000000),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(19, 9, 19, 2),
            child: Column(
              children: [
                Text(
                  "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit....  Stet clita..",
                  style: GoogleFonts.poppins(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff949494),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(19, 2, 0, 0),
            alignment: Alignment.centerLeft,
            child: Text(
              "07/08/2018",
              style: GoogleFonts.poppins(
                fontSize: 8,
                fontWeight: FontWeight.w400,
                color: Color(0xff949494),
              ),
            ),
          )
        ]));
  }

  Card buildCardSertifikat() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 15),
        shadowColor: Colors.black87,
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.fromLTRB(19, 31.8, 18, 0),
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      "Write about yourself",
                      style: GoogleFonts.poppins(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff000000),
                      ),
                    ),
                  ],
                ),
                Padding(padding: EdgeInsets.only(bottom: 9.2)),
                Text(
                  "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.... Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit et accusam et justo duo dolores et ea rebum.... Stet clita kasd gubergren, no sea",
                  style: GoogleFonts.poppins(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff949494),
                  ),
                ),
                Padding(padding: EdgeInsets.only(bottom: 18.6)),
              ],
            ),
          ),
          Divider(
            color: Colors.grey,
            endIndent: 17,
            indent: 17,
            // height: 17,
          ),
          Container(
            margin: EdgeInsets.fromLTRB(19.0, 19.0, 19.0, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Upload Certificates",
                  style: GoogleFonts.poppins(
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                    color: Color(0xff000000),
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: 500,
            // color: Colors.red,
            margin: EdgeInsets.fromLTRB(19, 9, 0, 17),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 8),
                        child: SizedBox(
                          width: 78,
                          height: 78,
                          child: ElevatedButton(
                            onPressed: () {},
                            child: SvgPicture.asset(iconAddBlue),
                            style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.symmetric(
                                  vertical: 8, horizontal: 30),
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5))),
                              primary: Color(0xffC6CEE3),
                            ),
                          ),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(right: 18)),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ]));
  }

  Card buildCardSLider() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 15),
        shadowColor: Colors.black87,
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            margin: EdgeInsets.fromLTRB(19.0, 19.0, 19.0, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Upload Pics",
                  style: GoogleFonts.poppins(
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                    color: Color(0xff000000),
                  ),
                ),
              ],
            ),
          ),
          Container(
            // color: Colors.red,
            margin: EdgeInsets.fromLTRB(19, 9, 0, 17),
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.end,
              children: [
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 8),
                        child: SizedBox(
                          width: 78,
                          height: 78,
                          child: ElevatedButton(
                            onPressed: () {},
                            child: SvgPicture.asset(iconAddBlue),
                            style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.symmetric(
                                  vertical: 8, horizontal: 30),
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5))),
                              primary: Color(0xffC6CEE3),
                            ),
                          ),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(right: 18)),
                      buildImg("01.jpeg"),
                      Padding(padding: EdgeInsets.only(right: 18)),
                      buildImg("02.jpeg"),
                      Padding(padding: EdgeInsets.only(right: 18)),
                      buildImg("03.jpeg"),
                      Padding(padding: EdgeInsets.only(right: 18)),
                      buildImg("04.jpeg"),
                      Padding(padding: EdgeInsets.only(right: 18)),
                      buildImg("05.jpeg"),
                      Padding(padding: EdgeInsets.only(right: 18)),
                      buildImg("06.jpeg"),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ]));
  }

  Card buildCardId() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 15),
        shadowColor: Colors.black87,
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.fromLTRB(19, 15, 18, 0),
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      "Upload ID",
                      style: GoogleFonts.poppins(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff000000),
                      ),
                    ),
                  ],
                ),
                Padding(padding: EdgeInsets.only(bottom: 9.2)),
                Text(
                  "Being ID verified can get you more jobs. This info is not shared with other users",
                  style: GoogleFonts.poppins(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff949494),
                  ),
                ),
                // Padding(padding: EdgeInsets.only(bottom: 18.6)),
              ],
            ),
          ),
          Container(
            width: 500,
            // color: Colors.red,
            margin: EdgeInsets.fromLTRB(19, 9, 0, 17),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 8),
                        child: SizedBox(
                          width: 78,
                          height: 78,
                          child: ElevatedButton(
                            onPressed: () {},
                            child: SvgPicture.asset(iconAddBlue),
                            style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.symmetric(
                                  vertical: 8, horizontal: 30),
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5))),
                              primary: Color(0xffC6CEE3),
                            ),
                          ),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(right: 18)),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ]));
  }

  Card buildCardAddVideo() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 15),
        shadowColor: Colors.black87,
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            margin: EdgeInsets.fromLTRB(19.0, 19.0, 19.0, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Add Video Link",
                  style: GoogleFonts.poppins(
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                    color: Color(0xff000000),
                  ),
                ),
              ],
            ),
          ),
          Container(
            // color: Colors.red,
            margin: EdgeInsets.fromLTRB(19, 9, 0, 17),
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.end,
              children: [
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 8),
                        child: SizedBox(
                          width: 78,
                          height: 78,
                          child: ElevatedButton(
                            onPressed: () {},
                            child: SvgPicture.asset(iconAddBlue),
                            style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.symmetric(
                                  vertical: 8, horizontal: 30),
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5))),
                              primary: Color(0xffC6CEE3),
                            ),
                          ),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(right: 18)),
                      buildImgVid1("Video - 1"),
                      Padding(padding: EdgeInsets.only(right: 18)),
                      buildImgVid2("Video - 2"),
                      Padding(padding: EdgeInsets.only(right: 18)),
                      buildImg("Video - 3"),
                      Padding(padding: EdgeInsets.only(right: 18)),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ]));
  }

  Card buildCardLinkedin() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 15),
        shadowColor: Colors.black87,
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.fromLTRB(19, 15, 18, 0),
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      "Add Video Link",
                      style: GoogleFonts.poppins(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff000000),
                      ),
                    ),
                  ],
                ),
                // Padding(padding: EdgeInsets.only(bottom: 9.2)),
              ],
            ),
          ),
          Container(
            width: 500,
            // color: Colors.red,
            margin: EdgeInsets.fromLTRB(19, 9, 0, 17),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 8),
                        child: SizedBox(
                          width: 78,
                          height: 78,
                          child: ElevatedButton(
                            onPressed: () {},
                            child: SvgPicture.asset(iconAddBlue),
                            style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.symmetric(
                                  vertical: 8, horizontal: 30),
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5))),
                              primary: Color(0xffC6CEE3),
                            ),
                          ),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(right: 18)),
                      buildImgVid1("Video - 1"),
                      Padding(padding: EdgeInsets.only(right: 18)),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ]));
  }
}
