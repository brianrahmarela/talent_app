import 'dart:math';
import 'dart:ui';

import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:talent_app/styles/icon.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'widget/slideable_myrequest.dart';
// import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class MyRequestHiring extends StatefulWidget {
  const MyRequestHiring({Key? key}) : super(key: key);

  @override
  _MyRequestHiringState createState() => _MyRequestHiringState();
}

class _MyRequestHiringState extends State<MyRequestHiring> {
  bool valServiceWalter = true;
  final List<Container> myList = List.generate(9, (index) {
    return Container(
      height: 50,
      width: 150,
      color: Color.fromARGB(255, Random().nextInt(256), Random().nextInt(256),
          Random().nextInt(256)),
    );
  });

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          extendBodyBehindAppBar: true,
          backgroundColor: Colors.white,
          body: Container(
            height: 760,
            // padding: EdgeInsets.only(bottom: 50),
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/bg_myjob.png'),
                fit: BoxFit.fill,
              ),
            ),
            child: SafeArea(
                child: ListView(
              children: [
                Container(
                  margin: EdgeInsets.fromLTRB(0, 46, 0, 15),
                  child: SlidableWidget(
                    onDismissed: (SlidableAction action) {
                      setState(() {
                        // items.removeAt(index);
                      });
                    },
                    key: Key(""),
                    child: Container(
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      child: buildCardTop(),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 15),
                  child: SlidableWidget(
                    onDismissed: (SlidableAction action) {
                      setState(() {
                        // items.removeAt(index);
                      });
                    },
                    key: Key(""),
                    child: Container(
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      child: buildCardButtom(),
                    ),
                  ),
                ),
                // GridView(
                //   gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                //       crossAxisCount: 3,
                //       crossAxisSpacing: 5,
                //       mainAxisSpacing: 10),
                //   children: [],
                // ),

                SizedBox(height: 55),
              ],
            )),
          ),
        ));
  }

  // Widget gridView(BuildContext context) => GridView.builder(
  //     gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
  //       mainAxisSpacing: 8,
  //       crossAxisSpacing: 8,
  //       crossAxisCount: 3,
  //     ),
  //     itemCount: 5,
  //     itemBuilder: (context, index) => buildImageCard(index));

  // Widget buildImageCard(int index) => Card(
  //       margin: EdgeInsets.all(8),
  //       child: ClipRRect(
  //           borderRadius: BorderRadius.circular(8),
  //           child: Image.asset('assets/images/')),
  //     );

  Card buildCardTop() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
        shadowColor: Colors.black87,
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            // color: Colors.amberAccent,
            margin: EdgeInsets.fromLTRB(19.0, 17.0, 19.0, 0),
            // width: 330,
            child: Row(
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  // color: Colors.black,
                  // height: 70,
                  child: Stack(
                    children: [
                      Positioned(
                          child: Container(
                        height: 62,
                        width: 59,
                        alignment: Alignment.centerLeft,
                        // margin: EdgeInsets.only(top: 15),
                        child: CircleAvatar(
                          backgroundImage:
                              AssetImage('assets/images/avatar_rating.png'),
                          radius: 60,
                        ),
                      )),
                      Positioned(
                          left: 0,
                          top: 36,
                          child: SvgPicture.asset(iconCheckGreen))
                    ],
                  ),
                ),
                Container(
                  // color: Color(0xffffffff),
                  margin: EdgeInsets.only(left: 8),
                  width: 200,
                  // color: Colors.red,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            "Clayton L.",
                            style: GoogleFonts.poppins(
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              color: Color(0xff000000),
                            ),
                          ),
                          Container(
                            height: 14,
                            child: SvgPicture.asset(iconAvatarSubscription),
                          ),
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(top: 5.8)),
                      Row(
                        // mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Icon(
                            Icons.star_rate,
                            color: Colors.yellow.shade700,
                            size: 14.0,
                          ),
                          Text(
                            " 5",
                            style: GoogleFonts.poppins(
                              fontSize: 11,
                              fontWeight: FontWeight.w600,
                              // color: Color(0xff33B440),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
                            child: SvgPicture.asset(iconLineVertical),
                          ),
                          Icon(
                            Icons.comment,
                            color: Color(0xff00A13C),
                            size: 14.0,
                          ),
                          Text(
                            " 3",
                            style: GoogleFonts.poppins(
                              fontSize: 11,
                              fontWeight: FontWeight.w600,
                              // color: Color(0xff33B440),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
                            child: SvgPicture.asset(iconLineVertical),
                          ),
                          Icon(
                            Icons.place,
                            color: Colors.red,
                            size: 14.0,
                          ),
                          Text(
                            " 3ml",
                            style: GoogleFonts.poppins(
                              fontSize: 11,
                              fontWeight: FontWeight.w600,
                              // color: Color(0xff33B440),
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 2),
                      ),
                      Row(
                        // mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            "14 Jobs done",
                            style: GoogleFonts.poppins(
                              fontSize: 10,
                              fontWeight: FontWeight.w400,
                              // color: Color(0xff33B440),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            // color: Colors.black,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: 220,
                  margin: EdgeInsets.fromLTRB(19, 9, 0, 0),
                  padding: EdgeInsets.fromLTRB(12, 2, 12, 2),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: Color(0xffF0BB3F),
                      borderRadius: BorderRadius.circular(8.0)),
                  constraints: BoxConstraints(maxWidth: 135, minHeight: 15),
                  child: Text.rich(
                    TextSpan(
                      text: "Plumber & 5 More",
                      style: GoogleFonts.poppins(
                        fontSize: 9,
                        fontWeight: FontWeight.w400,
                        color: Color(0xffffffff),
                      ),
                    ),
                  ),
                ),
                Container(
                  // width: 70,
                  margin: EdgeInsets.only(right: 17),
                  // color: Color(0xffF0BB3F),

                  child: Row(
                    children: [
                      ConstrainedBox(
                        constraints:
                            BoxConstraints.tightFor(width: 40, height: 40),
                        child: ElevatedButton(
                          onPressed: () {
                            // print('Hi there');
                          },
                          style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.all(0),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30))),
                          child: Ink(
                            decoration: BoxDecoration(
                                gradient: LinearGradient(colors: [
                                  Color(0xff5068A8),
                                  Color(0xff5CB5CF)
                                ]),
                                borderRadius: BorderRadius.circular(30)),
                            child: Container(
                              alignment: Alignment.center,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.favorite_border,
                                    color: Colors.white,
                                    textDirection: TextDirection.rtl,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(right: 8)),
                      Container(
                        width: 40,
                        child: ConstrainedBox(
                          constraints:
                              BoxConstraints.tightFor(width: 40, height: 40),
                          child: ElevatedButton(
                            onPressed: () {
                              // print('Hi there');
                            },
                            style: ElevatedButton.styleFrom(
                                padding: EdgeInsets.all(0),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(30))),
                            child: Ink(
                              decoration: BoxDecoration(
                                  gradient: LinearGradient(colors: [
                                    Color(0xff5068A8),
                                    Color(0xff5CB5CF)
                                  ]),
                                  borderRadius: BorderRadius.circular(30)),
                              child: Container(
                                alignment: Alignment.center,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.near_me_outlined,
                                      color: Colors.white,

                                      // textDirection: TextDirection.rtl,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            // color: Colors.amber,
            margin: EdgeInsets.fromLTRB(5, 5, 0, 7),
            padding: EdgeInsets.zero,
            child: Image(
              image: AssetImage('assets/images/myFavGallery.png'),
              // fit: BoxFit.fill,
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(19, 7, 19, 12),
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    flex: 2,
                    child: Text(
                      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit....  Stet clita..",
                      style: GoogleFonts.poppins(
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff949494),
                      ),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(right: 15)),
                  Flexible(
                    flex: 1,
                    child: Container(
                      // margin: EdgeInsets.fromLTRB(0, 10, 17, 13),
                      child: SizedBox(
                        width: 98,
                        height: 30,
                        child: ElevatedButton(
                          onPressed: () {
                            // print('Hi there');
                          },
                          style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.zero,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30))),
                          child: Ink(
                            decoration: BoxDecoration(
                                gradient: LinearGradient(colors: [
                                  Color(0xff5068A8),
                                  Color(0xff5CB5CF)
                                ]),
                                borderRadius: BorderRadius.circular(30)),
                            child: Container(
                              // margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
                              alignment: Alignment.center,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    'HIRE NOW',
                                    overflow: TextOverflow.ellipsis,
                                    style: GoogleFonts.poppins(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w600,
                                      color: Color(0xffffffff),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ]));
  }

  Card buildCardButtom() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
        shadowColor: Colors.black87,
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            // color: Colors.amberAccent,
            margin: EdgeInsets.fromLTRB(19.0, 17.0, 19.0, 0),
            // width: 330,
            child: Row(
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  // color: Colors.black,
                  // height: 70,
                  child: Stack(
                    children: [
                      Positioned(
                          child: Container(
                        height: 62,
                        width: 59,
                        alignment: Alignment.centerLeft,
                        // margin: EdgeInsets.only(top: 15),
                        child: CircleAvatar(
                          backgroundImage:
                              AssetImage('assets/images/adam1.png'),
                          radius: 60,
                        ),
                      )),
                      Positioned(
                          left: 0,
                          top: 36,
                          child: SvgPicture.asset(iconCheckGreen))
                    ],
                  ),
                ),
                Container(
                  // color: Color(0xffffffff),
                  margin: EdgeInsets.only(left: 8),
                  width: 200,
                  // color: Colors.red,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            "Christian.",
                            style: GoogleFonts.poppins(
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              color: Color(0xff000000),
                            ),
                          ),
                          Container(
                            height: 14,
                            child: SvgPicture.asset(iconAvatarSubscription),
                          ),
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(top: 5.8)),
                      Row(
                        // mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Icon(
                            Icons.star_rate,
                            color: Colors.yellow.shade700,
                            size: 14.0,
                          ),
                          Text(
                            " 5",
                            style: GoogleFonts.poppins(
                              fontSize: 11,
                              fontWeight: FontWeight.w600,
                              // color: Color(0xff33B440),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
                            child: SvgPicture.asset(iconLineVertical),
                          ),
                          Icon(
                            Icons.comment,
                            color: Color(0xff00A13C),
                            size: 14.0,
                          ),
                          Text(
                            " 3",
                            style: GoogleFonts.poppins(
                              fontSize: 11,
                              fontWeight: FontWeight.w600,
                              // color: Color(0xff33B440),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
                            child: SvgPicture.asset(iconLineVertical),
                          ),
                          Icon(
                            Icons.place,
                            color: Colors.red,
                            size: 14.0,
                          ),
                          Text(
                            " 3ml",
                            style: GoogleFonts.poppins(
                              fontSize: 11,
                              fontWeight: FontWeight.w600,
                              // color: Color(0xff33B440),
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 2),
                      ),
                      Row(
                        // mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            "14 Jobs done",
                            style: GoogleFonts.poppins(
                              fontSize: 10,
                              fontWeight: FontWeight.w400,
                              // color: Color(0xff33B440),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            // color: Colors.black,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: 220,
                  margin: EdgeInsets.fromLTRB(19, 9, 0, 0),
                  padding: EdgeInsets.fromLTRB(12, 2, 12, 2),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: Color(0xffF0BB3F),
                      borderRadius: BorderRadius.circular(8.0)),
                  constraints: BoxConstraints(maxWidth: 135, minHeight: 15),
                  child: Text.rich(
                    TextSpan(
                      text: "Home Exterior & 1 More",
                      style: GoogleFonts.poppins(
                        fontSize: 9,
                        fontWeight: FontWeight.w400,
                        color: Color(0xffffffff),
                      ),
                    ),
                  ),
                ),
                Container(
                  // width: 70,
                  margin: EdgeInsets.only(right: 17),
                  // color: Color(0xffF0BB3F),

                  child: Row(
                    children: [
                      ConstrainedBox(
                        constraints:
                            BoxConstraints.tightFor(width: 40, height: 40),
                        child: ElevatedButton(
                          onPressed: () {
                            // print('Hi there');
                          },
                          style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.all(0),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30))),
                          child: Ink(
                            decoration: BoxDecoration(
                                gradient: LinearGradient(colors: [
                                  Color(0xff5068A8),
                                  Color(0xff5CB5CF)
                                ]),
                                borderRadius: BorderRadius.circular(30)),
                            child: Container(
                              alignment: Alignment.center,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.favorite_border,
                                    color: Colors.white,
                                    textDirection: TextDirection.rtl,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(right: 8)),
                      Container(
                        width: 40,
                        child: ConstrainedBox(
                          constraints:
                              BoxConstraints.tightFor(width: 40, height: 40),
                          child: ElevatedButton(
                            onPressed: () {
                              // print('Hi there');
                            },
                            style: ElevatedButton.styleFrom(
                                padding: EdgeInsets.all(0),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(30))),
                            child: Ink(
                              decoration: BoxDecoration(
                                  gradient: LinearGradient(colors: [
                                    Color(0xff5068A8),
                                    Color(0xff5CB5CF)
                                  ]),
                                  borderRadius: BorderRadius.circular(30)),
                              child: Container(
                                alignment: Alignment.center,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.near_me_outlined,
                                      color: Colors.white,

                                      // textDirection: TextDirection.rtl,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            // color: Colors.amber,
            margin: EdgeInsets.fromLTRB(5, 5, 0, 7),
            padding: EdgeInsets.zero,
            child: Image(
              image: AssetImage('assets/images/myFavGallery2.png'),
              // fit: BoxFit.fill,
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(19, 7, 19, 12),
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    flex: 2,
                    child: Text(
                      "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit....  Stet clita..",
                      style: GoogleFonts.poppins(
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff949494),
                      ),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(right: 15)),
                  Flexible(
                    flex: 1,
                    child: Container(
                      // margin: EdgeInsets.fromLTRB(0, 10, 17, 13),
                      child: SizedBox(
                        width: 98,
                        height: 30,
                        child: ElevatedButton(
                          onPressed: () {
                            // print('Hi there');
                          },
                          style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.zero,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30))),
                          child: Ink(
                            decoration: BoxDecoration(
                                gradient: LinearGradient(colors: [
                                  Color(0xff5068A8),
                                  Color(0xff5CB5CF)
                                ]),
                                borderRadius: BorderRadius.circular(30)),
                            child: Container(
                              // margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
                              alignment: Alignment.center,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    'HIRE NOW',
                                    overflow: TextOverflow.ellipsis,
                                    style: GoogleFonts.poppins(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w600,
                                      color: Color(0xffffffff),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ]));
  }
}
