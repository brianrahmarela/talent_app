// import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
// import 'package:flutter_svg/flutter_svg.dart';

class Rating extends StatelessWidget {
  // const ({ Key? key }) : super(key: key);
  final TextEditingController controller =
      TextEditingController(text: "Type here");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Column(
            children: [
              Text("Hal Rating"),
            ],
          ),
        ),
        body: Container(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      spreadRadius: 3,
                      color: Color(0xffd3d3d3),
                      blurRadius: 10.0,
                      offset: Offset(0.0, 0.75))
                ],
              ),
              padding: EdgeInsets.only(top: 28.0),
              width: 330,

              // height: 157, //asli
              height: 204,
              // color: Colors.black,

              child: Stack(
                alignment: Alignment.center,
                children: [
                  profilePage("Shane Mendoza"),
                  myImages(
                      "https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1534&q=80"),
                  Positioned(
                    left: 130,
                    bottom: 20,
                    top: 0,
                    child: SizedBox(
                      height: 25,
                      width: 25,
                      child: CircleAvatar(
                        backgroundColor: Colors.green,
                        child: Icon(
                          Icons.check,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            buildCardNew(),
            SizedBox(
              width: 330,
              height: 43,
              child: ElevatedButton(
                onPressed: () {},
                child: Text('SUBMIT'),
                style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.symmetric(vertical: 8, horizontal: 30),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30))),
                ),
              ),
            ),
          ],
        )));
  }

  Card buildCardProfile(String text) {
    return Card(
      shadowColor: Colors.black87,
      elevation: 8,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            alignment: Alignment.center,
            height: 190,
            width: 330,
            padding: EdgeInsets.all(16),
            child: Text(
              text,
              style: GoogleFonts.poppins(
                fontSize: 14,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Card buildCardNew() {
    return Card(
      margin: EdgeInsets.all(40),
      shadowColor: Colors.black87,
      elevation: 8,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
      color: Colors.white,
      child: Container(
        margin: EdgeInsets.all(40),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          // mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text(
              'Give Your Feedback',
              // textAlign: TextAlign.left,
              overflow: TextOverflow.ellipsis,
              style: GoogleFonts.poppins(
                fontSize: 14,
                fontWeight: FontWeight.w700,
              ),
            ),
            TextField(
              controller: controller,
            ),
            // Text("data")
          ],
        ),
      ),
    );
  }

  Center profilePage(String profileName) {
    return Center(
      child: Padding(
        padding: EdgeInsets.only(top: 19.0),
        child: Container(
          decoration: BoxDecoration(
            color: Color(0xffffffff),
          ),
          child: Container(
            width: 330,
            height: 157,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12.0),
              // color: Color(0xff7c94b6),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(top: 56.0),
                      child: Text(
                        profileName,
                        style: GoogleFonts.poppins(
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),

                    // Padding(
                    //   padding: EdgeInsets.all(8.0),
                    //   child: Text(
                    //     jobDescription,
                    //   ),
                    //   ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Positioned myImages(String images) {
    return Positioned(
      top: 0.0,
      // left: 100.0,
      child: Container(
        width: 80.0,
        height: 80.0,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(fit: BoxFit.fill, image: NetworkImage(images)),
        ),
      ),
    );
  }
}
