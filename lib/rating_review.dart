import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:talent_app/styles/icon.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:talent_app/utils2.dart';
import 'package:talent_app/rating_review2.dart';

class RatingReview1 extends StatefulWidget {
  const RatingReview1({Key? key}) : super(key: key);

  @override
  _RatingReview2State createState() => _RatingReview2State();
}

class _RatingReview2State extends State<RatingReview1> {
  final TextEditingController controller =
      TextEditingController(text: "Type here");
  int indexTop = 0;
  double value = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          "Rating & Review",
          style: GoogleFonts.poppins(fontSize: 18, fontWeight: FontWeight.w600),
        ),
      ),
      body: Container(
        // margin: EdgeInsets.only(top: 1),
        // height: 760,
        // padding: EdgeInsets.only(bottom: 93),
        // SizedBox(height: 75),

        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/bg_appbar.png'),
            fit: BoxFit.fill,
          ),
        ),
        child: SafeArea(
            child: ListView(
          children: [
            SizedBox(height: 75),
            Stack(
              children: [
                Positioned(
                    // top: 30,
                    child: buildCardTop()),
                Positioned(
                    left: 141,
                    // top: 0,
                    child: Container(
                      height: 79,
                      width: 79,
                      // alignment: Alignment,
                      // margin: EdgeInsets.only(top: 15),
                      child: CircleAvatar(
                        backgroundImage:
                            AssetImage('assets/images/avatar_rating.png'),
                        radius: 60,
                      ),
                    )),
                Positioned(
                    left: 147, top: 61, child: SvgPicture.asset(iconCheckGreen))
              ],
            ),
            buildCardNew(),
            Container(
              width: 330,
              margin: EdgeInsets.fromLTRB(0, 87, 0, 37),
              padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
              child: ElevatedButton(
                onPressed: () => {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return RatingReview2();
                  })),
                },
                style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.zero,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20))),
                child: Ink(
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          colors: [Color(0xff5068A8), Color(0xff5CB5CF)]),
                      borderRadius: BorderRadius.circular(30)),
                  child: Container(
                    // width: 330,
                    // margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
                    height: 43,
                    alignment: Alignment.center,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'SUBMIT',
                          overflow: TextOverflow.ellipsis,
                          style: GoogleFonts.poppins(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            color: Color(0xffffffff),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        )),
      ),
    );
  }

  // Widget buildSlider() => SliderTheme(
  //       data: SliderThemeData(
  //         thumbShape: RoundSliderThumbShape(enabledThumbRadius: 12),
  //         thumbColor: Color(0xff5068A8),
  //         overlayColor: Color(0xff859DDC),
  //         valueIndicatorColor: Color(0xff5068A8),

  //         //track color
  //         activeTrackColor: Color(0xff5068A8),
  //         inactiveTrackColor: Color(0xffDEE4F5),

  //         //ticks in between
  //         activeTickMarkColor: Colors.red,
  //         inactiveTickMarkColor: Colors.transparent,
  //       ),
  //       child: SizedBox(
  //         child: Slider(
  //           value: value,
  //           min: 0,
  //           max: 5,
  //           divisions: 5,
  //           // activeColor: Color(0xff5068A8),
  //           // inactiveColor: Color(0xffDEE4F5),
  //           label: value.round().toString(),
  //           onChanged: (value) => setState(() => this.value = value),
  //         ),
  //       ),
  //  );

  Widget buildSliderTopLabel() {
    final labels = ['1', '2', '3', '4', '5'];
    final double min = 0;
    final double max = labels.length - 1.0;
    final divisions = labels.length - 1;

    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(horizontal: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: Utils.modelBuilder(
              labels,
              (index, label) {
                final selectedColor = Colors.black;
                final unselectedColor = Colors.black.withOpacity(0.3);
                final isSelected = index <= indexTop;
                final color = isSelected ? selectedColor : unselectedColor;

                return buildLabel(label: label, color: color, width: 30);
              },
            ),
          ),
        ),
        Slider(
          value: indexTop.toDouble(),
          min: min,
          max: max,
          divisions: divisions,
          activeColor: Color(0xff5068A8),
          inactiveColor: Color(0xffD7E1FA),
          label: labels[indexTop],
          onChanged: (value) => setState(() => this.indexTop = value.toInt()),
        ),
      ],
    );
  }

  Card buildCardTop() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 39, 15.0, 10),
        shadowColor: Colors.black87,
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        color: Colors.white,
        child: Column(children: [
          Container(
            margin: EdgeInsets.fromLTRB(0, 76.0, 0, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Shane Mendoza",
                  style: GoogleFonts.poppins(
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                    color: Color(0xff000000),
                  ),
                ),
              ],
            ),
          ),
          Padding(padding: EdgeInsets.only(bottom: 20.7)),
          Container(
            margin: EdgeInsets.fromLTRB(19, 9, 19, 2),
            child: Column(
              children: [
                // buildSlider(),
                buildSliderTopLabel(),
              ],
            ),
          ),
        ]));
  }

  Card buildCardNew() {
    return Card(
      margin: EdgeInsets.all(15),
      shadowColor: Colors.black87,
      elevation: 8,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
      color: Colors.white,
      child: Container(
        margin: EdgeInsets.all(40),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          // mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text(
              'Give Your Feedback',
              // textAlign: TextAlign.left,
              overflow: TextOverflow.ellipsis,
              style: GoogleFonts.poppins(
                fontSize: 14,
                fontWeight: FontWeight.w700,
              ),
            ),
            TextField(
              controller: controller,
            ),
            // Text("data")
          ],
        ),
      ),
    );
  }

  Widget buildLabel(
          {required label, required Color color, required double width}) =>
      Container(
        width: width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.asset(iconStar),
            Text(
              label,
              textAlign: TextAlign.center,
              style:
                  GoogleFonts.poppins(fontSize: 16, fontWeight: FontWeight.w600)
                      .copyWith(color: color),
            ),
          ],
        ),
      );
}
