import 'model/chat.dart';

class Data {
  static const chats = <Chat>[
    Chat(
      urlAvatar: 'assets/images/avatar_rating.png',
      username: 'Michael Y.',
      message: 'Sure',
      time: '2.22 PM',
    ),
    Chat(
      urlAvatar: 'assets/images/adam1.png',
      username: 'Thealha Y.',
      message: 'Lorem Ipsum Dolor lala...',
      time: '5.37 PM',
    ),
    Chat(
      urlAvatar: 'assets/images/louis1.png',
      username: 'Honey',
      message: 'Hey whatsup?',
      time: 'Yesterday',
    ),
    Chat(
      urlAvatar: 'assets/images/avatar_profile.png',
      username: 'Family',
      message: 'Good morning guys!',
      time: 'Yesterday',
    ),
    Chat(
      urlAvatar: 'assets/images/avatar_rating.png',
      username: 'Book Club',
      message: 'What\'s the next book?',
      time: 'Yesterday',
    ),
  ];
}
