import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:talent_app/styles/icon.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Clayton extends StatefulWidget {
  const Clayton({Key? key}) : super(key: key);

  @override
  _ClaytonState createState() => _ClaytonState();
}

class _ClaytonState extends State<Clayton> {
  bool val1 = true;
  bool valServiceProvider = true;
  bool valServiceWalter = true;
  bool valServicePainting = true;
  bool valServiceDogWalking = true;

  onChangeFunction1(bool newValue1) {
    val1 = newValue1;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        titleSpacing: 0.0,
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Row(children: [
          Container(
            child: Row(
              children: [
                Stack(
                  children: [
                    Positioned(
                        child: Container(
                      height: 62,
                      width: 50,
                      alignment: Alignment.centerLeft,
                      child: CircleAvatar(
                        backgroundImage:
                            AssetImage('assets/images/avatar_rating.png'),
                        radius: 20,
                      ),
                    )),
                    Positioned(
                        left: -3,
                        top: 36,
                        child: SizedBox(
                            height: 17,
                            child: SvgPicture.asset(iconCheckGreen)))
                  ],
                ),
                Row(
                  children: [
                    Text(
                      "Clayton L.",
                      style: GoogleFonts.poppins(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Color(0xffffffff),
                      ),
                    ),
                    Container(
                      height: 14,
                      child: SvgPicture.asset(iconAvatarSubscription),
                    ),
                    Padding(padding: EdgeInsets.only(right: 101)),
                    Icon(
                      Icons.favorite,
                      color: Colors.white,
                      textDirection: TextDirection.rtl,
                    ),
                    Padding(padding: EdgeInsets.only(right: 23)),
                    Icon(
                      Icons.near_me,
                      color: Colors.white,
                      textDirection: TextDirection.rtl,
                    ),
                  ],
                )
              ],
            ),
          )
        ]),
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/bg_appbar.png'),
            fit: BoxFit.fill,
          ),
        ),
        child: SafeArea(
            child: SizedBox(
          child: ListView(
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(0, 35, 0, 21),
                child: Image(
                  image: AssetImage('assets/images/gallery.png'),
                  // fit: BoxFit.fill,
                ),
              ),
              // buildCardGalleryImg(),
              buildCardJobsDone(),
              buildCardAreaOfService(),
              buildCardAddVideo(),
              buildCardWeb(),
              buildCardLinkedin(),
              Container(
                margin: EdgeInsets.fromLTRB(17, 32.0, 17, 9),
                child: SizedBox(
                  child: ElevatedButton(
                    onPressed: () {
                      // print('Hi there');
                    },
                    style: ElevatedButton.styleFrom(
                        padding: EdgeInsets.zero,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20))),
                    child: Ink(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              colors: [Color(0xff5068A8), Color(0xff5CB5CF)]),
                          borderRadius: BorderRadius.circular(30)),
                      child: Container(
                        // width: 330,
                        // margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
                        height: 43,
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'MARK JOB AS COMPLETED',
                              overflow: TextOverflow.ellipsis,
                              style: GoogleFonts.poppins(
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                                color: Color(0xffffffff),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                // color: Colors.white,
                margin: EdgeInsets.fromLTRB(17, 0, 17, 14),
                child: SizedBox(
                  // width: 330,
                  // height: 43,
                  child: ElevatedButton(
                    onPressed: () {},
                    child: Text('RATE A REVIEW'),
                    style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        onPrimary: Colors.grey,
                        side: BorderSide(
                            color: Colors.grey,
                            width: 1,
                            style:
                                BorderStyle.solid), //set border for the button

                        padding:
                            EdgeInsets.symmetric(vertical: 8, horizontal: 30),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(30)),
                        ),
                        textStyle: GoogleFonts.poppins(
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                        )),
                  ),
                ),
              ),
              Container(
                child: Row(
                  children: [
                    Container(
                      alignment: Alignment.center,
                      // color: Colors.red,
                      margin: EdgeInsets.fromLTRB(19, 0, 0, 51),
                      child: (Row(
                          // mainAxisAlignment: MainAxisAlignment.center,
                          // crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: 10),
                            Padding(
                              padding: EdgeInsets.only(right: 6),
                              child: SvgPicture.asset(iconFlags),
                            ),
                            Text(
                              "Report Trevor S.",
                              style: GoogleFonts.poppins(
                                fontSize: 12,
                                fontWeight: FontWeight.w500,
                                color: Color(0xff5068A8),
                              ),
                            ),
                            // Text(
                            //   text2,
                            //   style: GoogleFonts.poppins(
                            //     fontSize: 12,
                            //     fontWeight: FontWeight.w400,
                            //     color: Color(0xff949494),
                            //   ),
                            // ),
                          ])),
                    ),
                  ],
                ),
              )
            ],
          ),
        )),
      ),
    );
  }

  Widget chipForRow(String label, Color color) {
    return Chip(
      labelPadding: EdgeInsets.fromLTRB(12.0, 1.0, 12.0, 0.7),
      backgroundColor: Colors.orange.shade300,
      padding: EdgeInsets.all(6.0),
      label: Text(
        label,
        style: TextStyle(color: Colors.white),
      ),
    );
  }

  Card buildCardJobsDone() {
    return Card(
      margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 15),
      shadowColor: Colors.black87,
      elevation: 8,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      color: Colors.white,
      child: Column(
        // mainAxisAlignment: MainAxisAlignment.center,
        // crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(
            // mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(4.0, 15.0, 19.0, 18.0),
              ),
              Text(
                "14",
                style: GoogleFonts.poppins(
                  fontSize: 14,
                  fontWeight: FontWeight.w600,
                  color: Color(0xff33B440),
                ),
              ),
              Padding(padding: EdgeInsets.fromLTRB(0.0, 50.0, 5.0, 0.0)),
              Text(
                " Jobs Done",
                style: GoogleFonts.poppins(
                  fontSize: 14,
                  fontWeight: FontWeight.w600,
                ),
              ),
              SizedBox(
                width: 100.0,
              ),
              Icon(
                Icons.star_rate,
                color: Colors.yellow.shade700,
                size: 14.0,
              ),
              Text(
                " 5",
                style: GoogleFonts.poppins(
                  fontSize: 12,
                  fontWeight: FontWeight.w600,
                  // color: Color(0xff33B440),
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
                child: SvgPicture.asset(iconLineVertical),
              ),
              Icon(
                Icons.message,
                color: Colors.green.shade500,
                size: 14.0,
              ),
              Text(
                "  3",
                style: GoogleFonts.poppins(
                  fontSize: 12,
                  fontWeight: FontWeight.w600,
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
                child: SvgPicture.asset(iconLineVertical),
              ),
              Icon(
                Icons.location_pin,
                color: Colors.red.shade500,
                size: 14.0,
              ),
              Text(
                " 3 ml ",
                style: GoogleFonts.poppins(
                  fontSize: 12,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(19.0, 15.0, 19.0, 18.0),
            child: Text(
              "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem dolor sit....",
              style: GoogleFonts.poppins(
                  fontSize: 13.7,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff212121)),
              overflow: TextOverflow.ellipsis,
              maxLines: 7,
            ),
          ),
        ],
      ),
    );
  }

  Card buildCardAreaOfService() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 15),
        shadowColor: Colors.black87,
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.fromLTRB(19, 15, 18, 10),
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.center,
              // crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(padding: EdgeInsets.fromLTRB(10.0, 30.0, 0.0, 0.0)),
                    Text("Area of Service",
                        style: GoogleFonts.poppins(
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                          color: Color(0xff000000),
                        ))
                  ],
                ),
                Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 5, right: 15.0),
                      child: chipForRow('plumber & 5 more', Colors.white),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 15.0),
                      child: chipForRow('plumber & 5 more', Colors.white),
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 5, right: 15.0),
                      child: chipForRow('mounting & installing', Colors.white),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 10.0),
                      child: chipForRow('painting', Colors.white),
                    ),
                  ],
                ),
                Stack(children: [
                  Positioned(
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(left: 5, right: 15.0),
                          child: chipForRow('Fencing', Colors.white),
                        ),
                        Padding(
                          padding: EdgeInsets.only(right: 15.0),
                          child: chipForRow('Hair', Colors.white),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    // height: 70,
                    alignment: Alignment.bottomRight,
                    // color: Color(0xffaeaeae),
                    child: Positioned(
                      // top: 10,
                      left: 290,
                      child: ConstrainedBox(
                        constraints:
                            BoxConstraints.tightFor(width: 54, height: 54),
                        child: ElevatedButton(
                          onPressed: () {
                            // print('Hi there');
                          },
                          style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.all(0),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30))),
                          child: Ink(
                            decoration: BoxDecoration(
                                gradient: LinearGradient(colors: [
                                  Color(0xff5068A8),
                                  Color(0xff5CB5CF)
                                ]),
                                borderRadius: BorderRadius.circular(30)),
                            child: Container(
                              alignment: Alignment.center,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SvgPicture.asset(iconChat),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ]),

                // )
              ],
            ),
          ),
        ]));
  }

  Widget buildImg(String text) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              Positioned(
                child: Container(
                  width: 99,
                  height: 99,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/images/img_track_now.png')),
                  ),
                ),
              ),
              Positioned(
                top: 5,
                left: 72,
                child: CircleAvatar(
                  radius: 10,
                  backgroundColor: Color(0xff5068A8),
                ),
              ),
              Positioned(
                left: 66.5,
                child: SizedBox(
                  height: 14,
                  width: 14,
                  child: IconButton(
                    color: Color(0xffffffff),
                    onPressed: () {},
                    icon: Icon(
                      Icons.close,
                      size: 14.0,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(left: 13),
            child: Text(
              text,
              // textAlign: TextAlign.left,
              style: GoogleFonts.poppins(
                fontSize: 11,
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
        ],
      ),
      // child:
    );
  }

  Widget buildImgVid1(String text) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              Positioned(
                child: Container(
                  width: 99,
                  height: 99,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/images/video-1.png')),
                  ),
                ),
              ),
              Positioned(
                top: 5,
                left: 72,
                child: CircleAvatar(
                  radius: 10,
                  backgroundColor: Color(0xff5068A8),
                ),
              ),
              Positioned(
                left: 66.5,
                child: SizedBox(
                  height: 14,
                  width: 14,
                  child: IconButton(
                    color: Color(0xffffffff),
                    onPressed: () {},
                    icon: Icon(
                      Icons.close,
                      size: 14.0,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(left: 13),
            child: Text(
              text,
              // textAlign: TextAlign.left,
              style: GoogleFonts.poppins(
                fontSize: 11,
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
        ],
      ),
      // child:
    );
  }

  Widget buildImgVid2(String text) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              Positioned(
                child: Container(
                  width: 99,
                  height: 99,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/images/video-2.png')),
                  ),
                ),
              ),
              Positioned(
                top: 5,
                left: 72,
                child: CircleAvatar(
                  radius: 10,
                  backgroundColor: Color(0xff5068A8),
                ),
              ),
              Positioned(
                left: 66.5,
                child: SizedBox(
                  height: 14,
                  width: 14,
                  child: IconButton(
                    color: Color(0xffffffff),
                    onPressed: () {},
                    icon: Icon(
                      Icons.close,
                      size: 14.0,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(left: 13),
            child: Text(
              text,
              // textAlign: TextAlign.left,
              style: GoogleFonts.poppins(
                fontSize: 11,
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
        ],
      ),
      // child:
    );
  }

  Widget buildImgWeb1(String text) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              Positioned(
                child: Container(
                  width: 99,
                  height: 99,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/images/web-1.png')),
                  ),
                ),
              ),
              Positioned(
                top: 5,
                left: 72,
                child: CircleAvatar(
                  radius: 10,
                  backgroundColor: Color(0xff5068A8),
                ),
              ),
              Positioned(
                left: 66.5,
                child: SizedBox(
                  height: 14,
                  width: 14,
                  child: IconButton(
                    color: Color(0xffffffff),
                    onPressed: () {},
                    icon: Icon(
                      Icons.close,
                      size: 14.0,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(left: 13),
            child: Text(
              text,
              // textAlign: TextAlign.left,
              style: GoogleFonts.poppins(
                fontSize: 11,
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
        ],
      ),
      // child:
    );
  }

  Widget buildImgLinkedin(String text) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              Positioned(
                child: Container(
                  width: 99,
                  height: 99,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/images/linkedinfoto.png')),
                  ),
                ),
              ),
              Positioned(
                top: 5,
                left: 72,
                child: CircleAvatar(
                  radius: 10,
                  backgroundColor: Color(0xff5068A8),
                ),
              ),
              Positioned(
                left: 66.5,
                child: SizedBox(
                  height: 14,
                  width: 14,
                  child: IconButton(
                    color: Color(0xffffffff),
                    onPressed: () {},
                    icon: Icon(
                      Icons.close,
                      size: 14.0,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(left: 13),
            child: Text(
              text,
              // textAlign: TextAlign.left,
              style: GoogleFonts.poppins(
                fontSize: 11,
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
        ],
      ),
      // child:
    );
  }

  Card buildCardTop() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
        shadowColor: Colors.black87,
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            // color: Colors.amberAccent,
            margin: EdgeInsets.fromLTRB(19.0, 17.0, 19.0, 0),
            // width: 330,
            child: Row(
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  // color: Colors.black,
                  // height: 70,
                  child: Stack(
                    children: [
                      Positioned(
                          child: Container(
                        height: 62,
                        width: 59,
                        alignment: Alignment.centerLeft,
                        // margin: EdgeInsets.only(top: 15),
                        child: CircleAvatar(
                          backgroundImage:
                              AssetImage('assets/images/avatar_rating.png'),
                          radius: 60,
                        ),
                      )),
                      Positioned(
                          left: 0,
                          top: 36,
                          child: SvgPicture.asset(iconCheckGreen))
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 8),
                  width: 200,
                  // color: Colors.red,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            "Clayton L.",
                            style: GoogleFonts.poppins(
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              color: Color(0xff000000),
                            ),
                          ),
                          Container(
                            height: 14,
                            child: SvgPicture.asset(iconAvatarSubscription),
                          ),
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(top: 5.8)),
                      Row(
                        // mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SvgPicture.asset(iconrating_4_5),
                        ],
                      ),
                      Row(
                        // mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            width: 220,
                            margin: EdgeInsets.fromLTRB(0, 9, 0, 0),
                            padding: EdgeInsets.fromLTRB(12, 2, 12, 2),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                color: Color(0xffF0BB3F),
                                borderRadius: BorderRadius.circular(8.0)),
                            constraints:
                                BoxConstraints(maxWidth: 135, minHeight: 15),
                            child: Text.rich(
                              TextSpan(
                                text: "Plumber & 5 More",
                                style: GoogleFonts.poppins(
                                  fontSize: 9,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xffffffff),
                                ),
                              ),
                            ),
                          )

                          // ],
                          // ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(19.0, 19.0, 19.0, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Vestibulum",
                  style: GoogleFonts.poppins(
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                    color: Color(0xff000000),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(19, 9, 19, 2),
            child: Column(
              children: [
                Text(
                  "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit....  Stet clita..",
                  style: GoogleFonts.poppins(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff949494),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(19, 2, 0, 0),
            alignment: Alignment.centerLeft,
            child: Text(
              "07/08/2018",
              style: GoogleFonts.poppins(
                fontSize: 8,
                fontWeight: FontWeight.w400,
                color: Color(0xff949494),
              ),
            ),
          )
        ]));
  }

  Card buildCardSertifikat() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 15),
        shadowColor: Colors.black87,
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.fromLTRB(19, 31.8, 18, 0),
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      "Write about yourself",
                      style: GoogleFonts.poppins(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff000000),
                      ),
                    ),
                  ],
                ),
                Padding(padding: EdgeInsets.only(bottom: 9.2)),
                Text(
                  "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.... Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit et accusam et justo duo dolores et ea rebum.... Stet clita kasd gubergren, no sea",
                  style: GoogleFonts.poppins(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff949494),
                  ),
                ),
                Padding(padding: EdgeInsets.only(bottom: 18.6)),
              ],
            ),
          ),
          Divider(
            color: Colors.grey,
            endIndent: 17,
            indent: 17,
            // height: 17,
          ),
          Container(
            margin: EdgeInsets.fromLTRB(19.0, 19.0, 19.0, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Upload Certificates",
                  style: GoogleFonts.poppins(
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                    color: Color(0xff000000),
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: 500,
            // color: Colors.red,
            margin: EdgeInsets.fromLTRB(19, 9, 0, 17),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 8),
                        child: SizedBox(
                          width: 78,
                          height: 78,
                          child: ElevatedButton(
                            onPressed: () {},
                            child: SvgPicture.asset(iconAddBlue),
                            style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.symmetric(
                                  vertical: 8, horizontal: 30),
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5))),
                              primary: Color(0xffC6CEE3),
                            ),
                          ),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(right: 18)),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ]));
  }

  Card buildCardId() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 15),
        shadowColor: Colors.black87,
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.fromLTRB(19, 15, 18, 0),
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      "Upload ID",
                      style: GoogleFonts.poppins(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff000000),
                      ),
                    ),
                  ],
                ),
                Padding(padding: EdgeInsets.only(bottom: 9.2)),
                Text(
                  "Being ID verified can get you more jobs. This info is not shared with other users",
                  style: GoogleFonts.poppins(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff949494),
                  ),
                ),
                // Padding(padding: EdgeInsets.only(bottom: 18.6)),
              ],
            ),
          ),
          Container(
            width: 500,
            // color: Colors.red,
            margin: EdgeInsets.fromLTRB(19, 9, 0, 17),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 8),
                        child: SizedBox(
                          width: 78,
                          height: 78,
                          child: ElevatedButton(
                            onPressed: () {},
                            child: SvgPicture.asset(iconAddBlue),
                            style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.symmetric(
                                  vertical: 8, horizontal: 30),
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5))),
                              primary: Color(0xffC6CEE3),
                            ),
                          ),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(right: 18)),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ]));
  }

  Card buildCardGalleryImg() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 15),
        shadowColor: Colors.black87,
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            // color: Color(0xff000000),

            // margin: EdgeInsets.fromLTRB(19, 24.6, 19, 15.0),
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/gallery.png'),
                fit: BoxFit.fill,
              ),
            ),
          ),
        ]));
  }

  Card buildCardAddVideo() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 15),
        shadowColor: Colors.black87,
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            margin: EdgeInsets.fromLTRB(19.0, 19.0, 19.0, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Video Link",
                  style: GoogleFonts.poppins(
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                    color: Color(0xff000000),
                  ),
                ),
              ],
            ),
          ),
          Container(
            // color: Colors.red,
            margin: EdgeInsets.fromLTRB(19, 9, 0, 17),
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.end,
              children: [
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 8),
                        child: SizedBox(
                          width: 78,
                          height: 78,
                          child: ElevatedButton(
                            onPressed: () {},
                            child: SvgPicture.asset(iconAddBlue),
                            style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.symmetric(
                                  vertical: 8, horizontal: 30),
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5))),
                              primary: Color(0xffC6CEE3),
                            ),
                          ),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(right: 18)),
                      buildImgVid1("Video - 1"),
                      Padding(padding: EdgeInsets.only(right: 18)),
                      buildImgVid2("Video - 2"),
                      Padding(padding: EdgeInsets.only(right: 18)),
                      buildImg("Video - 3"),
                      Padding(padding: EdgeInsets.only(right: 18)),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ]));
  }

  Card buildCardWeb() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 15),
        shadowColor: Colors.black87,
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.fromLTRB(19, 15, 18, 0),
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      "Website Link",
                      style: GoogleFonts.poppins(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff000000),
                      ),
                    ),
                  ],
                ),
                // Padding(padding: EdgeInsets.only(bottom: 9.2)),
              ],
            ),
          ),
          Container(
            width: 500,
            // color: Colors.red,
            margin: EdgeInsets.fromLTRB(19, 9, 0, 17),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 8),
                        child: SizedBox(
                          width: 78,
                          height: 78,
                          child: ElevatedButton(
                            onPressed: () {},
                            child: SvgPicture.asset(iconAddBlue),
                            style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.symmetric(
                                  vertical: 8, horizontal: 30),
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5))),
                              primary: Color(0xffC6CEE3),
                            ),
                          ),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(right: 18)),
                      buildImgWeb1("web-1.png"),
                      Padding(padding: EdgeInsets.only(right: 18)),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ]));
  }

  Card buildCardLinkedin() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
        shadowColor: Colors.black87,
        elevation: 8,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            alignment: Alignment.topLeft,
            margin: EdgeInsets.fromLTRB(19, 15, 18, 0),
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      "Linkedin Profile Link",
                      style: GoogleFonts.poppins(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff000000),
                      ),
                    ),
                  ],
                ),
                // Padding(padding: EdgeInsets.only(bottom: 9.2)),
              ],
            ),
          ),
          Container(
            width: 500,
            // color: Colors.red,
            margin: EdgeInsets.fromLTRB(19, 9, 0, 17),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 8),
                        child: SizedBox(
                          width: 78,
                          height: 78,
                          child: ElevatedButton(
                            onPressed: () {},
                            child: SvgPicture.asset(iconAddBlue),
                            style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.symmetric(
                                  vertical: 8, horizontal: 30),
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5))),
                              primary: Color(0xffC6CEE3),
                            ),
                          ),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(right: 18)),
                      buildImgLinkedin("Linkedin"),
                      Padding(padding: EdgeInsets.only(right: 18)),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ]));
  }
}
