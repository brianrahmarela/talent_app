// import 'package:flutter/material.dart';

const iconCheck = 'assets/svg/check_24px.svg';
const iconWallet = 'assets/svg/wallet.svg';
const iconPaypalFigma = 'assets/svg/paypal_figma.svg';
const iconAdd = 'assets/svg/add_icon.svg';
const iconAddBlue = 'assets/svg/add_blue.svg';
const iconAvatarSubscription = 'assets/svg/avatar_subscription.svg';
const iconCamera = 'assets/svg/icon_camera.svg';
const iconCheckGreen = 'assets/svg/check_green_circle.svg';
const iconLineVertical = 'assets/svg/line_vertical.svg';
const iconrating_4_5 = 'assets/svg/rating_4.5.svg';
const iconStar = 'assets/svg/star.svg';
const iconPlace = 'assets/svg/place.svg';
const iconTv = 'assets/svg/tv.svg';
const iconCook = 'assets/svg/cook.svg';
const iconFlags = 'assets/svg/flags.svg';
const iconChat = 'assets/svg/chat.svg';
const iconOutlineGreen = 'assets/svg/check_outline_green.svg';
const iconBlock = 'assets/svg/block.svg';
const iconAddUser = 'assets/svg/add_user.svg';
const iconMicrophone = 'assets/svg/microphone.svg';
const iconLocationDisable = 'assets/svg/location_disable.svg';
const iconCalendarFoogle = 'assets/svg/calendar_foogle.svg';
const iconLineVerticalLong = 'assets/svg/line_vertical_long.svg';
const iconGoogleMaps = 'assets/svg/google_maps.svg';
