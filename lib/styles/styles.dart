import 'package:division/division.dart';
import 'package:flutter/material.dart';

ParentStyle parentStyle = ParentStyle()
  ..width(78)
  ..height(78)
  ..borderRadius(all: 10)
  ..margin(all: 4)
  ..elevation(3)
  ..background.color(Colors.amber);
