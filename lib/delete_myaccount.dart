import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class DeleteMyAccount extends StatefulWidget {
  const DeleteMyAccount({Key? key}) : super(key: key);

  @override
  _DeleteMyAccountState createState() => _DeleteMyAccountState();
}

enum SingingCharacter {
  toomanyemail,
  acchacked,
  interest,
  madeanotheracc,
  dontuse,
  ihavepriv,
  other
}

class _DeleteMyAccountState extends State<DeleteMyAccount> {
// ...

  SingingCharacter? _character = SingingCharacter.toomanyemail;
  // SingingCharacter? _char = SingingCharacter.interest;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          "Settings",
          style: GoogleFonts.poppins(fontSize: 18, fontWeight: FontWeight.w600),
        ),
      ),
      body: SingleChildScrollView(
          child: Container(
        height: 860,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/bg_appbar.png'),
            fit: BoxFit.fill,
          ),
        ),
        child: SafeArea(
            child: Center(
                child: Column(
          children: [
            Container(
              // color: Colors.amber,
              margin: EdgeInsets.fromLTRB(0, 68.0, 0, 0),
              child: buildCardChangeLang(
                  "Are you sure you want to delete \nyour account?",
                  "Please let us know why you are leaving."),
              width: 530,
              // height: 120,
            ),
            // Divider(
            //   color: Colors.grey,
            //   // height: 17,
            // ),
            // height: 17,
            Container(
              margin: EdgeInsets.only(top: 23),
              child: Column(
                children: <Widget>[
                  RadioListTile<SingingCharacter>(
                    title: Text(
                        'I\'m getting too many email notifications (Did you know you can change your Notification settings)',
                        style: GoogleFonts.poppins(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            color: Color(0xff949494))),
                    value: SingingCharacter.toomanyemail,
                    groupValue: _character,
                    onChanged: (SingingCharacter? value) {
                      setState(() {
                        _character = value;
                      });
                    },
                  ),
                  RadioListTile<SingingCharacter>(
                    title: Text('My Account was hacked',
                        style: GoogleFonts.poppins(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            color: Color(0xff949494))),
                    value: SingingCharacter.acchacked,
                    groupValue: _character,
                    onChanged: (SingingCharacter? value) {
                      setState(() {
                        _character = value;
                      });
                    },
                  ),
                  RadioListTile<SingingCharacter>(
                    title: Text('I can\'t find interesting jobs',
                        style: GoogleFonts.poppins(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            color: Color(0xff949494))),
                    value: SingingCharacter.interest,
                    groupValue: _character,
                    onChanged: (SingingCharacter? value) {
                      setState(() {
                        _character = value;
                      });
                    },
                  ),
                  RadioListTile<SingingCharacter>(
                    title: Text('I accidently made another acoount',
                        style: GoogleFonts.poppins(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            color: Color(0xff949494))),
                    value: SingingCharacter.madeanotheracc,
                    groupValue: _character,
                    onChanged: (SingingCharacter? value) {
                      setState(() {
                        _character = value;
                      });
                    },
                  ),
                  RadioListTile<SingingCharacter>(
                    title: Text('I don\'t use or like the app or website',
                        style: GoogleFonts.poppins(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            color: Color(0xff949494))),
                    value: SingingCharacter.dontuse,
                    groupValue: _character,
                    onChanged: (SingingCharacter? value) {
                      setState(() {
                        _character = value;
                      });
                    },
                  ),
                  RadioListTile<SingingCharacter>(
                    title: Text('I have a privacy concern',
                        style: GoogleFonts.poppins(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            color: Color(0xff949494))),
                    value: SingingCharacter.ihavepriv,
                    groupValue: _character,
                    onChanged: (SingingCharacter? value) {
                      setState(() {
                        _character = value;
                      });
                    },
                  ),
                  RadioListTile<SingingCharacter>(
                    title: Text('other',
                        style: GoogleFonts.poppins(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            color: Color(0xff949494))),
                    value: SingingCharacter.other,
                    groupValue: _character,
                    onChanged: (SingingCharacter? value) {
                      setState(() {
                        _character = value;
                      });
                    },
                  ),
                  Container(
                      // width: 310,
                      // height: 59,
                      // color: Colors.amber,
                      margin: EdgeInsets.fromLTRB(27, 20, 45, 50.4),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('Write your reason',
                              textAlign: TextAlign.left,
                              style: GoogleFonts.poppins(
                                fontSize: 10,
                                fontWeight: FontWeight.w500,
                                color: Color(0xff949494),
                              )),
                          TextField(
                            decoration: InputDecoration(
                              hintText: 'Type here',
                              // labelText: 'Type here',
                              labelStyle: GoogleFonts.poppins(
                                fontSize: 12,
                                fontWeight: FontWeight.w500,
                                color: Color(0xffB2B2B2),
                              ),
                            ),
                            keyboardType: TextInputType.visiblePassword,
                            // textInputAction: TextInputAction.done,
                            obscureText: true,
                          ),
                        ],
                      )),
                ],
              ),
            ),
          ],
        ))),
      )),
    );
  }

  Card buildCardChangeLang(String text1, String text2) {
    return Card(
      margin: EdgeInsets.fromLTRB(10.0, 0, 15.0, 0),
      shadowColor: Colors.black87,
      elevation: 0,
      color: Colors.transparent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
      child: Container(
          // color: Colors.amber,
          child: Row(
        // crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Container(
              margin: EdgeInsets.only(left: 19),
              child: (Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      text1,
                      style: GoogleFonts.poppins(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff212121),
                      ),
                    ),
                    SizedBox(height: 11),
                    Text(
                      text2,
                      style: GoogleFonts.poppins(
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff949494),
                      ),
                    ),
                  ])),
            ),
          ),
        ],
      )),
    );
  }
}
