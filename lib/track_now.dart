import 'dart:ui';

import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:talent_app/styles/icon.dart';
import 'package:flutter_svg/flutter_svg.dart';

class TrackNow extends StatefulWidget {
  const TrackNow({Key? key}) : super(key: key);

  @override
  _TrackNowState createState() => _TrackNowState();
}

class _TrackNowState extends State<TrackNow> {
  bool valServiceWalter = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          "Track Now",
          style: GoogleFonts.poppins(fontSize: 18, fontWeight: FontWeight.w600),
        ),
      ),
      body: Container(
        height: 760,
        // padding: EdgeInsets.only(bottom: 50),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/bg_appbar.png'),
            fit: BoxFit.fill,
          ),
        ),
        child: SafeArea(
            child: ListView(
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(0, 53, 0, 0),
              child: buildCardSelectService(),
            ),
            Container(
              // alignment: Alignment.topCenter,
              margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
              child: buildCardSelectService(),
              // width: 530,
              // height: 178,
            ),
            Container(
              // alignment: Alignment.topCenter,
              margin: EdgeInsets.fromLTRB(0, 15, 0, 57),
              child: buildCardSelectService(),
              // width: 530,
              // height: 178,
            ),
            // SizedBox(height: 15),
          ],
        )),
      ),
    );
  }

  Card buildCardSelectService() {
    return Card(
      margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
      shadowColor: Colors.black87,
      elevation: 8,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
      color: Colors.white,
      child: Column(
        children: [
          buildCardWalterNeedCook("Need Cook", "Status: Active"),
          Container(
            // height: 100,
            child: Row(
              children: [
                Container(
                  // alignment: Alignment.topLeft,
                  // color: Colors.blue,
                  width: 381,
                  child: Row(
                    children: [
                      Image(
                        image: AssetImage('assets/images/img_track_now.png'),
                        // fit: BoxFit.fill,
                      ),
                      Column(
                        children: [
                          Container(
                            width: 230,
                            // color: Colors.amber,
                            // margin: EdgeInsets.fromLTRB(0, 17.0, 19.0, 0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                  child: Text(
                                    "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit",
                                    maxLines: 3,
                                    overflow: TextOverflow.ellipsis,
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Color(0xff000000),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(padding: EdgeInsets.only(top: 15)),
                          Container(
                            width: 230,
                            // color: Colors.amber,
                            // margin: EdgeInsets.fromLTRB(19.0, 17.0, 19.0, 0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(right: 9),
                                  child: SvgPicture.asset(iconTv),
                                ),
                                Text(
                                  "Before the 08 Nov 2018.",
                                  style: GoogleFonts.poppins(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500,
                                    color: Color(0xff000000),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 230,
                            // color: Colors.amber,
                            // margin: EdgeInsets.fromLTRB(19.0, 17.0, 19.0, 0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(children: [
                                  Padding(
                                    padding: EdgeInsets.only(right: 9),
                                    child: SvgPicture.asset(iconPlace),
                                  ),
                                  Text(
                                    "3 ml",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500,
                                      color: Color(0xff000000),
                                    ),
                                  )
                                ]),
                                SizedBox(height: 32),
                                Row(children: [
                                  Text(
                                    "Budget:",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w600,
                                      color: Color(0xff000000),
                                    ),
                                  ),
                                  Text(
                                    "\$240",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w600,
                                      color: Color(0xff33B440),
                                    ),
                                  ),
                                ]),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Card buildCardWalterNeedCook(String text1, String text2) {
    return Card(
      margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
      shadowColor: Colors.black87,
      elevation: 0,
      // color: Colors.green,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
      child: Container(
          // color: Colors.amber,
          child: Row(
        // crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Row(
              children: [
                Container(
                  alignment: Alignment.center,
                  // color: Colors.red,
                  margin: EdgeInsets.only(left: 19),
                  child: (Row(
                      // mainAxisAlignment: MainAxisAlignment.center,
                      // crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 10),
                        Text(
                          text1,
                          style: GoogleFonts.poppins(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            color: Color(0xff000000),
                          ),
                        ),
                        // Text(
                        //   text2,
                        //   style: GoogleFonts.poppins(
                        //     fontSize: 12,
                        //     fontWeight: FontWeight.w400,
                        //     color: Color(0xff949494),
                        //   ),
                        // ),
                      ])),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 8),
                  child: SvgPicture.asset(iconCook),
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(0, 10, 17, 13),
            child: SizedBox(
              // width: 330,
              height: 30,
              child: ElevatedButton(
                onPressed: () {
                  // print('Hi there');
                },
                style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.zero,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30))),
                child: Ink(
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          colors: [Color(0xff5068A8), Color(0xff5CB5CF)]),
                      borderRadius: BorderRadius.circular(30)),
                  child: Container(
                    width: 107,
                    // margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
                    height: 30,
                    alignment: Alignment.center,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Track Now',
                          overflow: TextOverflow.ellipsis,
                          style: GoogleFonts.poppins(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            color: Color(0xffffffff),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      )),
    );
  }

  Widget buildSwitchServiceWalter() => Transform.scale(
        scale: 0.6,
        child: CupertinoSwitch(
          activeColor: Color(0xff5068A8),
          value: valServiceWalter,
          onChanged: (valServiceWalter) =>
              setState(() => this.valServiceWalter = valServiceWalter),
        ),
      );
}
