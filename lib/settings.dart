import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:talent_app/choose_language.dart';
import 'package:talent_app/delete_myaccount.dart';

import './styles/icon.dart';

class Settings extends StatefulWidget {
  const Settings({Key? key}) : super(key: key);

  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  bool valNotification = true;
  bool valJobAlert = true;
  bool valAccountActive = true;
  bool valChangeTheLanguange = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          "Settings",
          style: GoogleFonts.poppins(fontSize: 18, fontWeight: FontWeight.w600),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          // height: 760,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/bg_appbar.png'),
              fit: BoxFit.fill,
            ),
          ),
          child: SafeArea(
              child: Center(
                  child: Column(
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(0, 73.0, 0, 20.0),
                child: buildCardNotification("Notification", ""),
                width: 530,
                height: 25,
              ),
              Divider(
                color: Colors.grey,
                // height: 17,
              ),
              Container(
                margin: EdgeInsets.fromLTRB(0, 20.0, 0, 20.0),
                child: buildCardJobAlert("Job Alerts", ""),
                width: 530,
                height: 25,
              ),
              Divider(
                color: Colors.grey,
                // height: 17,
              ),
              Container(
                margin: EdgeInsets.fromLTRB(0, 18.0, 0, 18.0),
                child: buildCardChangePass("Change Password", ""),
                width: 530,
                height: 32,
              ),
              Divider(
                color: Colors.grey,
                // height: 17,
              ),
              Container(
                margin: EdgeInsets.fromLTRB(0, 20.0, 0, 20.0),
                child: buildCardAccountActive("Account Active", ""),
                width: 530,
                height: 25,
              ),
              Divider(
                color: Colors.grey,
                // height: 17,
              ),
              Container(
                // color: Colors.black26,
                margin: EdgeInsets.fromLTRB(0, 20.0, 0, 20.0),
                child: buildCardChangeLang("Change the Language", "English"),
                width: 530,
                height: 40,
              ),
              Divider(
                color: Colors.grey,
                // height: 17,
              ),
              Container(
                margin: EdgeInsets.fromLTRB(0, 18.0, 0, 18.0),
                child: buildCardDeleteAcc("Delete My Account", ""),
                width: 530,
                height: 32,
              ),
              Divider(
                color: Colors.grey,
              ),
              SizedBox(
                height: 259,
              ),
            ],
          ))),
        ),
      ),
    );
  }

  Widget buildSwitchNotification() => Transform.scale(
        scale: 0.7,
        child: CupertinoSwitch(
          activeColor: Color(0xff5068A8),
          value: valNotification,
          onChanged: (valNotification) =>
              setState(() => this.valNotification = valNotification),
        ),
      );

  Widget buildSwitchJobAlert() => Transform.scale(
        scale: 0.7,
        child: CupertinoSwitch(
          activeColor: Color(0xff5068A8),
          value: valJobAlert,
          onChanged: (valJobAlert) =>
              setState(() => this.valJobAlert = valJobAlert),
        ),
      );

  Widget buildSwitchAccountActive() => Transform.scale(
        scale: 0.7,
        child: CupertinoSwitch(
          activeColor: Color(0xff5068A8),
          value: valAccountActive,
          onChanged: (valAccountActive) =>
              setState(() => this.valAccountActive = valAccountActive),
        ),
      );

  Widget _buildPopupDialog(BuildContext context) {
    return AlertDialog(
      insetPadding: EdgeInsets.all(15),
      contentPadding: EdgeInsets.all(0),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(6))),
      // backgroundColor: Colors.amber,
      // title: const Text('Enter Current Password'),
      // title: Divider(
      //   color: Colors.black,
      //   // height: 17,
      // ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            margin: EdgeInsets.fromLTRB(0, 15, 0, 13.6),
            child: Text("Enter Current Password",
                style: GoogleFonts.poppins(
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                )),
          ),
          Divider(
            color: Colors.black,
            // height: 17,
          ),
          Container(
            alignment: Alignment.center,
            margin: EdgeInsets.fromLTRB(42, 15, 42, 34),
            child: Text("For security reasons, please verify your password",
                textAlign: TextAlign.center,
                style: GoogleFonts.poppins(
                  fontSize: 12,
                  fontWeight: FontWeight.w600,
                  color: Color(0xff949494),
                )),
          ),
          Container(
              width: 269.34,
              // height: 59,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Password',
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                        fontSize: 10,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff949494),
                      )),
                  TextField(
                    decoration: InputDecoration(
                      hintText: 'Type here',
                      // labelText: 'Type here',
                      labelStyle: GoogleFonts.poppins(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: Color(0xffB2B2B2),
                      ),
                    ),
                    keyboardType: TextInputType.visiblePassword,
                    // textInputAction: TextInputAction.done,
                    obscureText: true,
                  ),
                ],
              )),
          Container(
            alignment: Alignment.centerRight,
            margin: EdgeInsets.fromLTRB(0, 5.5, 30, 0),
            child: Text('Forgot Password',
                // textAlign: TextAlign.right,
                style: GoogleFonts.poppins(
                  fontSize: 10,
                  fontWeight: FontWeight.w500,
                  color: Color(0xff4F78E4),
                )),
          ),
          Container(
            // color: Color,
            height: 51,
            margin: EdgeInsets.only(top: 14),
            padding: EdgeInsets.zero,
            child: Row(
              children: [
                Flexible(
                  flex: 1,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    style: ElevatedButton.styleFrom(
                        padding: EdgeInsets.zero,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20))),
                    child: Ink(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              colors: [Color(0xffE4E4E4), Color(0xffB2B2B2)]),
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(6))),
                      child: Container(
                        // width: 378,
                        margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        // height: 47,
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'CANCEL',
                              overflow: TextOverflow.ellipsis,
                              style: GoogleFonts.poppins(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                color: Color(0xff212121),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Flexible(
                  flex: 1,

                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    style: ElevatedButton.styleFrom(
                        padding: EdgeInsets.zero,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20))),
                    child: Ink(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              colors: [Color(0xff5068A8), Color(0xff5CB5CF)]),
                          borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(6))),
                      child: Container(
                        // width: 378,
                        // margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
                        padding: EdgeInsets.zero,
                        // height: 47,
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'VERIFY',
                              overflow: TextOverflow.ellipsis,
                              style: GoogleFonts.poppins(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                color: Color(0xffffffff),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  // ),
                ),
              ],
            ),
          )
        ],
      ),
      // actions: <Widget>[
      //   TextButton(
      //     onPressed: () {
      //       Navigator.of(context).pop();
      //     },
      //     child: const Text('CANCEL'),
      //   ),
      //   TextButton(
      //     onPressed: () {
      //       Navigator.of(context).pop();
      //     },
      //     child: const Text('ACCEPT'),
      //   ),
      // ],
    );
  }

  Card buildCardNotification(String text1, String text2) {
    return Card(
      margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
      shadowColor: Colors.black87,
      elevation: 0,
      color: Colors.transparent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
      child: Container(
          // color: Colors.amber,
          child: Row(
        children: [
          Expanded(
            child: Container(
              margin: EdgeInsets.only(left: 19),
              child: (Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Notification",
                      style: GoogleFonts.poppins(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff000000),
                      ),
                    ),
                  ])),
            ),
          ),
          buildSwitchNotification(),
        ],
      )),
    );
  }

  Card buildCardJobAlert(String text1, String text2) {
    return Card(
      margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
      shadowColor: Colors.black87,
      elevation: 0,
      color: Colors.transparent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
      child: Container(
          child: Row(
        children: [
          Expanded(
            child: Container(
              margin: EdgeInsets.only(left: 19),
              child: (Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Job Alerts",
                      style: GoogleFonts.poppins(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff000000),
                      ),
                    ),
                  ])),
            ),
          ),
          buildSwitchJobAlert(),
        ],
      )),
    );
  }

  Card buildCardChangePass(String text1, String text2) {
    return Card(
      margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
      shadowColor: Colors.black87,
      elevation: 0,
      color: Colors.transparent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
      child: Container(
          child: Row(
        children: [
          Expanded(
            child: Container(
              margin: EdgeInsets.only(left: 19),
              child: (Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Change Password",
                      style: GoogleFonts.poppins(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff000000),
                      ),
                    ),
                  ])),
            ),
          ),
          IconButton(
            icon: new Icon(
              Icons.arrow_forward_ios,
              size: 17,
              color: Color(0xffB2B2B2),
            ),
            onPressed: () {
              showDialog(
                context: context,
                builder: (BuildContext context) => _buildPopupDialog(context),
              );
            },
          ),
        ],
      )),
    );
  }

  Card buildCardAccountActive(String text1, String text2) {
    return Card(
      margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
      shadowColor: Colors.black87,
      elevation: 0,
      color: Colors.transparent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
      child: Container(
          child: Row(
        children: [
          Expanded(
            child: Container(
              margin: EdgeInsets.only(left: 19),
              child: (Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // Text(
                    //   "Delete My Account,
                    //   style: GoogleFonts.poppins(
                    //     fontSize: 14,
                    //     fontWeight: FontWeight.w600,
                    //     color: Color(0xff000000),
                    //   ),
                    // ),
                    Text(
                      "Account Active",
                      style: GoogleFonts.poppins(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff000000),
                      ),
                    ),
                  ])),
            ),
          ),
          buildSwitchAccountActive(),
        ],
      )),
    );
  }

  Card buildCardChangeLang(String text1, String text2) {
    return Card(
      margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
      shadowColor: Colors.black87,
      elevation: 0,
      color: Colors.transparent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
      child: Container(
          // color: Colors.amber,
          child: Row(
        // crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Container(
              margin: EdgeInsets.only(left: 19),
              child: (Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Change the Language",
                      style: GoogleFonts.poppins(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff000000),
                      ),
                    ),
                    Text(
                      "English",
                      style: GoogleFonts.poppins(
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff949494),
                      ),
                    ),
                  ])),
            ),
          ),
          IconButton(
            icon: new Icon(
              Icons.arrow_forward_ios,
              size: 17,
              color: Color(0xffB2B2B2),
            ),
            onPressed: () => {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return ChooseLanguage();
              })),
            },
          ),
        ],
      )),
    );
  }

  Card buildCardDeleteAcc(String text1, String text2) {
    return Card(
      margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
      shadowColor: Colors.black87,
      elevation: 0,
      color: Colors.transparent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
      // color: Colors.white,
      child: Container(
          // color: Colors.amber,
          child: Row(
        // crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Container(
              margin: EdgeInsets.only(left: 19),
              child: (Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // Text(
                    //   "Delete My Account,
                    //   style: GoogleFonts.poppins(
                    //     fontSize: 14,
                    //     fontWeight: FontWeight.w600,
                    //     color: Color(0xff000000),
                    //   ),
                    // ),
                    Text(
                      "Delete My Account",
                      style: GoogleFonts.poppins(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff000000),
                      ),
                    ),
                  ])),
            ),
          ),
          IconButton(
            icon: new Icon(
              Icons.arrow_forward_ios,
              size: 17,
              color: Color(0xffB2B2B2),
            ),
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return DeleteMyAccount();
              }));
            },
          ),
        ],
      )),
    );
  }

  Card buildCardHiddenTop() {
    return Card(
        margin: EdgeInsets.fromLTRB(0, 26, 0, 30),
        elevation: 0,
        color: Colors.transparent,
        // shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
        child: Column(children: [
          Container(
            // margin: EdgeInsets.only(top: 17.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.only(bottom: 5),
                  child: SvgPicture.asset(iconAvatarSubscription),
                ),
                Text(
                  "You are Premium Member",
                  style: GoogleFonts.poppins(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                    color: Color(0xffffffff),
                  ),
                ),
                // Text(
                //   "31 Oct 2018",
                //   style: GoogleFonts.poppins(
                //     fontSize: 20,
                //     fontWeight: FontWeight.bold,
                //     color: Color(0xff000000),
                //   ),
                // ),
              ],
            ),
          ),
        ]));
  }
}
