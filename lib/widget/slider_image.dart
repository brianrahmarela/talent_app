import 'package:division/division.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:talent_app/styles/styles.dart';

class SliderImage extends StatefulWidget {
  final String url;
  SliderImage(this.url);

  @override
  _SliderImageState createState() => _SliderImageState();
}

class _SliderImageState extends State<SliderImage> {
  @override
  Widget build(BuildContext context) {
    return Parent(
      style: parentStyle.clone()
        ..background.image(url: widget.url, fit: BoxFit.cover),
    );
  }
}
