import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:google_fonts/google_fonts.dart';

enum SlidableAction { archive, share, more, delete }

class SlidableWidget<T> extends StatelessWidget {
  final Widget child;
  final Function(SlidableAction action) onDismissed;

  const SlidableWidget({
    required this.child,
    required this.onDismissed,
    required Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Slidable(
          actionPane: SlidableDrawerActionPane(),
          child: child,
          actionExtentRatio: 0.25,

          /// left side
          actions: <Widget>[
            Card(
              margin: EdgeInsets.zero,
              shadowColor: Colors.black87,
              elevation: 4,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              child: ElevatedButton(
                onPressed: () {},
                style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.zero,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10))),
                child: Ink(
                  // padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          colors: [Color(0xffA80303), Color(0xffFF0000)]),
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(10),
                          bottomRight: Radius.circular(10))),
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.close,
                              size: 30,
                              color: Colors.white,
                              textDirection: TextDirection.rtl,
                            ),
                            Padding(padding: EdgeInsets.only(bottom: 10)),
                            Text(
                              "Reject",
                              style: GoogleFonts.poppins(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                color: Color(0xffffffff),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            // IconSlideAction(
            //   caption: 'Share',
            //   color: Colors.indigo,
            //   icon: Icons.share,
            //   onTap: () => onDismissed(SlidableAction.share),
            // ),
          ],

          /// right side
          secondaryActions: <Widget>[
            Card(
              margin: EdgeInsets.zero,
              shadowColor: Colors.black87,
              elevation: 4,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              child: ElevatedButton(
                onPressed: () {},
                style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.zero,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10))),
                child: Ink(
                  // padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          colors: [Color(0xff0C7005), Color(0xff0DA502)]),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          bottomLeft: Radius.circular(10))),
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.check,
                              size: 30,
                              color: Colors.white,
                              textDirection: TextDirection.rtl,
                            ),
                            Padding(padding: EdgeInsets.only(bottom: 10)),
                            Text(
                              "Accept",
                              style: GoogleFonts.poppins(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                color: Color(0xffffffff),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ]);
}
