import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_svg/svg.dart';
import 'package:talent_app/styles/icon.dart';
import 'package:google_fonts/google_fonts.dart';

enum SlidableAction { archive, share, more, delete }

class SlidableWidget<T> extends StatelessWidget {
  final Widget child;
  final Function(SlidableAction action) onDismissed;

  const SlidableWidget({
    required this.child,
    required this.onDismissed,
    required Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) =>
      Slidable(actionPane: SlidableDrawerActionPane(), child: child,
          // actionExtentRatio: 0.3,

          /// left side
          // actions: <Widget>[
          //   IconSlideAction(
          //     caption: 'Archive',
          //     color: Colors.blue,
          //     icon: Icons.archive,
          //     onTap: () => onDismissed(SlidableAction.archive),
          //   ),
          //   IconSlideAction(
          //     caption: 'Share',
          //     color: Colors.indigo,
          //     icon: Icons.share,
          //     onTap: () => onDismissed(SlidableAction.share),
          //   ),
          // ],

          /// right side
          secondaryActions: <Widget>[
            // child,
            // IconSlideAction(
            //   caption: 'More',
            //   color: Colors.black45,
            //   icon: Icons.more_horiz,
            //   onTap: () => onDismissed(SlidableAction.more),
            // ),

            Card(
              margin: EdgeInsets.zero,
              shadowColor: Colors.black87,
              elevation: 4,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              child: ElevatedButton(
                onPressed: () {},
                style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.zero,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10))),
                child: Ink(
                  // padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          colors: [Color(0xffA80303), Color(0xffFF0000)]),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          bottomLeft: Radius.circular(10))),
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SvgPicture.asset(iconBlock),
                            Text(
                              'BLOCK',
                              style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.w600,
                                color: Color(0xffffffff),
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ]);
}
