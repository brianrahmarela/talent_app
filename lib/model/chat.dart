// import 'package:meta/meta.dart';

class Chat {
  final String urlAvatar;
  final String username;
  final String message;
  final String time;

  const Chat({
    required this.urlAvatar,
    required this.username,
    required this.message,
    required this.time,
  });
}
