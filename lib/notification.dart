import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:talent_app/styles/icon.dart';
import 'package:flutter_svg/flutter_svg.dart';

class NotificationPage extends StatefulWidget {
  const NotificationPage({Key? key}) : super(key: key);

  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          "Notification",
          style: GoogleFonts.poppins(fontSize: 18, fontWeight: FontWeight.w600),
        ),
      ),
      body: Container(
        height: 760,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/bg_appbar.png'),
            fit: BoxFit.fill,
          ),
        ),
        child: SafeArea(
            child: ListView(
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(0, 36, 15, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  ElevatedButton(
                    onPressed: () => {},
                    child: Text(
                      "Mark all as read",
                      style: GoogleFonts.poppins(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff212121),
                      ),
                    ),
                    style: ElevatedButton.styleFrom(
                      padding: EdgeInsets.all(0),
                      primary: Colors.transparent,
                      // onPrimary: Colors.black,
                      shadowColor: Colors.transparent,
                    ),
                  )
                ],
              ),
            ),
            buildCardTop(),
            SizedBox(height: 10),
            buildCardTop2(),
            SizedBox(height: 10),
            buildCardSquare(),
            SizedBox(height: 10),
            buildCardSquare(),
            SizedBox(height: 10),
            buildCardBottom(),
            SizedBox(height: 10),
            buildCardBottom(),
            SizedBox(height: 10),
            buildCardBottom(),
            SizedBox(height: 60),
          ],
        )),
      ),
    );
  }

  Card buildCardTop() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
        shadowColor: Colors.black87,
        elevation: 4,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            // color: Colors.amberAccent,
            margin: EdgeInsets.fromLTRB(19.0, 0, 19.0, 0),
            child: Row(
              children: [
                Container(
                  alignment: Alignment.center,
                  height: 80,
                  child: Stack(
                    children: [
                      Positioned(
                          child: Container(
                        height: 62,
                        width: 59,
                        child: CircleAvatar(
                          backgroundImage:
                              AssetImage('assets/images/avatar_rating.png'),
                          radius: 60,
                        ),
                      )),
                      Positioned(
                          left: 0,
                          top: 36,
                          child: SvgPicture.asset(iconCheckGreen))
                    ],
                  ),
                ),
                Stack(
                  children: [
                    Positioned(
                      child: Container(
                          // color: Color(0xffaeaeaea),
                          width: 279,
                          margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(15.0, 0, 19.0, 0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    RichText(
                                      text: TextSpan(
                                        // Note: Styles for TextSpans must be explicitly defined.
                                        // Child text spans will inherit styles from parent
                                        style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xff5068A8),
                                          letterSpacing: 0.3,
                                        ),
                                        children: <TextSpan>[
                                          TextSpan(
                                              text:
                                                  'You have a new message \nfrom'),
                                          TextSpan(
                                              text: ' Warren Pratt',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w600)),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: 5),
                                      // color: Color(0xffdddddd),
                                      child: Text(
                                        "On 06/18/2018 at 08.00 PM",
                                        style: GoogleFonts.poppins(
                                          fontSize: 10.5,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xff212121),
                                          letterSpacing: 0.3,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          )),
                    ),
                    Positioned(
                      top: 28,
                      left: 275,
                      child: Padding(
                        padding: EdgeInsets.only(left: 0),
                        child: Positioned(
                          right: 0,
                          bottom: 0,
                          child: SizedBox(
                            height: 8.5,
                            width: 8.5,
                            child: CircleAvatar(
                              backgroundColor: Colors.red,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ]));
  }

  Card buildCardTop2() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
        shadowColor: Colors.black87,
        elevation: 4,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            // color: Colors.amberAccent,
            margin: EdgeInsets.fromLTRB(19.0, 0, 19.0, 0),
            child: Row(
              children: [
                Container(
                  alignment: Alignment.center,
                  height: 80,
                  child: Stack(
                    children: [
                      Positioned(
                          child: Container(
                        height: 62,
                        width: 59,
                        child: CircleAvatar(
                          backgroundImage:
                              AssetImage('assets/images/avatar_rating.png'),
                          radius: 60,
                        ),
                      )),
                      Positioned(
                          left: 0,
                          top: 36,
                          child: SvgPicture.asset(iconCheckGreen))
                    ],
                  ),
                ),
                Stack(
                  children: [
                    Positioned(
                      child: Container(
                          // color: Color(0xffaeaeaea),
                          width: 279,
                          margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(15.0, 0, 19.0, 0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    RichText(
                                      text: TextSpan(
                                        // Note: Styles for TextSpans must be explicitly defined.
                                        // Child text spans will inherit styles from parent
                                        style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xff5068A8),
                                          letterSpacing: 0.3,
                                        ),
                                        children: <TextSpan>[
                                          TextSpan(
                                              text:
                                                  'You have a new message \nfrom'),
                                          TextSpan(
                                              text: ' Alvin Dunn',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w600)),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: 5),
                                      // color: Color(0xffdddddd),
                                      child: Text(
                                        "On 06/18/2018 at 08.00 PM",
                                        style: GoogleFonts.poppins(
                                          fontSize: 10.5,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xff212121),
                                          letterSpacing: 0.3,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          )),
                    ),
                    Positioned(
                      top: 28,
                      left: 275,
                      child: Padding(
                        padding: EdgeInsets.only(left: 0),
                        child: Positioned(
                          right: 0,
                          bottom: 0,
                          child: SizedBox(
                            height: 8.5,
                            width: 8.5,
                            child: CircleAvatar(
                              backgroundColor: Colors.red,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ]));
  }

  Card buildCardSquare() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
        shadowColor: Colors.black87,
        elevation: 4,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            // color: Colors.amberAccent,
            margin: EdgeInsets.fromLTRB(19.0, 0, 19.0, 0),
            child: Row(
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  alignment: Alignment.center,
                  height: 80,
                  child: Stack(
                    children: [
                      Positioned(
                        child: Container(
                          margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          width: 64,
                          // height: 54,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage(
                                    'assets/images/adam_square.png')),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Stack(
                  children: [
                    Positioned(
                      child: Container(
                          // color: Color(0xffaeaeaea),
                          width: 279,
                          margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(15.0, 0, 19.0, 0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    RichText(
                                      text: TextSpan(
                                        // Note: Styles for TextSpans must be explicitly defined.
                                        // Child text spans will inherit styles from parent
                                        style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xff212121),
                                          letterSpacing: 0.3,
                                        ),
                                        children: <TextSpan>[
                                          TextSpan(text: 'You got a new'),
                                          TextSpan(
                                              text: ' Job Request',
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w600)),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: 5),
                                      // color: Color(0xffdddddd),
                                      child: Text(
                                        "On 06/18/2018 at 08.00 PM",
                                        style: GoogleFonts.poppins(
                                          fontSize: 10.5,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xff212121),
                                          letterSpacing: 0.3,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          )),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ]));
  }

  Card buildCardBottom() {
    return Card(
        margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 0),
        shadowColor: Colors.black87,
        elevation: 4,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Column(children: [
          Container(
            // color: Colors.amberAccent,
            margin: EdgeInsets.fromLTRB(19.0, 0, 19.0, 0),
            child: Row(
              children: [
                Container(
                  alignment: Alignment.center,
                  height: 80,
                  child: Stack(
                    children: [
                      Positioned(
                          child: Container(
                        height: 62,
                        width: 59,
                        child: CircleAvatar(
                          backgroundImage:
                              AssetImage('assets/images/avatar_rating.png'),
                          radius: 60,
                        ),
                      )),
                    ],
                  ),
                ),
                Stack(
                  children: [
                    Positioned(
                      child: Container(
                          // color: Color(0xffaeaeaea),
                          width: 279,
                          margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(15.0, 0, 19.0, 0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    RichText(
                                      text: TextSpan(
                                        // Note: Styles for TextSpans must be explicitly defined.
                                        // Child text spans will inherit styles from parent
                                        style: GoogleFonts.poppins(
                                          fontSize: 13,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xff212121),
                                          letterSpacing: 0.3,
                                        ),
                                        children: <TextSpan>[
                                          TextSpan(
                                              text:
                                                  'You have a new message \nfrom'),
                                          TextSpan(
                                              text: ' Warren Pratt',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w600)),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: 5),
                                      // color: Color(0xffdddddd),
                                      child: Text(
                                        "On 06/18/2018 at 08.00 PM",
                                        style: GoogleFonts.poppins(
                                          fontSize: 10.5,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xff212121),
                                          letterSpacing: 0.3,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          )),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ]));
  }
}
